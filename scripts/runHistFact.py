import os
import subprocess



#lq_masses = ['300', '500', '900', '1300', '1700']
lq_masses = ['300']
#cuts = [ 'wZMassLowerCut','woZMassLowerCut','woZMassLowerCut_wEtmissCut','NewVarSet_wEtmissCut', 'NewVarSet_woEtmissCut']
cuts = [ 'NewVarSet_wEtmissCut']
for Mass in lq_masses:
    for Cut in cuts:
        input_dir  = "input/normfiles/LeptoQuarkLepHad_9thAug2019.9thAug2019_HH_13TeV_9thAug2019_" + Cut + "_lephad_LQ3_BDT_" + Mass + "/"
        input_file = "Norm_BMin0_incJet1_dist" + Mass + "_J2_D" + Cut.replace('_','') + "SRLQ3BDT_T1_SpcTauLH_Y2015_LTT0_L1.root"

        command='MakeSimpleModel.cxx("' + input_dir + '","' + input_file + '","' + Mass + '")'
        subprocess.call(["root", "-l", "-b", "-q", command])
        #subprocess.call(['ls', input_dir+input_file])

