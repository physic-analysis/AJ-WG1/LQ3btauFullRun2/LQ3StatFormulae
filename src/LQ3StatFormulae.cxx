
#include "LQ3StatFormulae.h"

LQ3StatFormulae::LQ3StatFormulae()
{}

LQ3StatFormulae::LQ3StatFormulae(TString infile, TString workspaceName, TString modelConfigName, TString dataName)
{
    // Open the RooWorkspace
    TFile* fin = new TFile(infile, "read");
    m_workspace = static_cast<RooWorkspace*>(fin->Get(workspaceName));
    if (!m_workspace) {
        std::cerr <<  "ERROR::Workspace: " << workspaceName <<  " doesn't exist!" << std::endl;
        exit(1);
    }

    // Get the model config
    m_model_config = static_cast<RooStats::ModelConfig*>(m_workspace->obj(modelConfigName));
    if (!m_model_config){
        std::cerr << "ERROR::ModelConfig: " << modelConfigName << " doesn't exist!" << std::endl;
        exit(1);
    }
    m_POI = static_cast<RooRealVar*>(m_model_config->GetParametersOfInterest()->first());

    // Get the data set
    m_dataset = (RooDataSet*)m_workspace->data(dataName);
    if (!m_dataset ) {
        std::cerr << "ERROR::Dataset: " <<  dataName <<  " doesn't exist!" << std::endl;
        exit(1);
    }
    
    // Find injected mu before anything moves the PoI (for second case)
    std::cout << "L." << __LINE__ <<  " : Find injected mu." << std::endl;
    mu_inj = 1.;
    if(doInj) {     
        if ( m_workspace->var("ATLAS_norm_muInjection") ) {
            mu_inj = m_workspace->var("ATLAS_norm_muInjection")->getVal();
        } else {
            mu_inj = m_workspace->var("SigXsecOverSM")->getVal(); 
        }
    }

    /* Set the variables */
    betterBands             = 1;                // (recommendation = 1) improve bands by using a more appropriate asimov dataset for those points
    betterNegativeBands     = 0;                // (recommendation = 0) improve also the negative bands
    profileNegativeAtZero   = 0;                // (recommendation = 0) profile asimov for negative bands at zero
    defaultMinimizer        = "Minuit2";        // or "Minuit"
    defaultPrintLevel       = 1;                // Minuit print level
    defaultStrategy         = 1;                // Minimization strategy. 0-2. 0 = fastest, least robust. 2 = slowest, most robust
    killBelowFatal          = 1;                // In case you want to suppress RooFit warnings further, set to 1
    doBlind                 = true;             // in case your analysis is blinded
    conditionalExpected     = 1 && !doBlind;    // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
    doTilde                 = true;             // bound mu at zero if true and do the \tilde{q}_{mu} asymptotics
    doExp                   = true;             // compute expected limit
    doObs                   = true && !doBlind; // compute observed limit
    doInj                   = true;             // compute expected limit after signal injection
    precision               = 0.005;            // % precision in mu that defines iterative cutoff
    verbose                 = 0;                // 1 = very spammy
    usePredictiveFit        = 1;                // experimental, extrapolate best fit nuisance parameters based on previous fit results
    extrapolateSigma        = 1;                // experimantal, extrapolate sigma based on previous fits
    maxRetries              = 3;                // number of minimize(fcn) retries before giving up
    CL                      = 0.95;
    target_CLs              = 1-CL;
    nrMinimize              = 0;
    direction               = 1;
}

LQ3StatFormulae::~LQ3StatFormulae()
{
}

void LQ3StatFormulae::CreateObservedNLL()
{
    m_obs_nll = createNLL( getDataSet() );
    m_map_snapshots[m_obs_nll] = "nominalGlobs";
    m_map_data_nll[m_dataset]  = m_obs_nll;
    m_workspace->saveSnapshot("nominalGlobs",*m_model_config->GetGlobalObservables());
    m_workspace->saveSnapshot("nominalNuis",( m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet()));
}

void LQ3StatFormulae::CreateAsimov()
{
  m_asimovData_0 = makeAsimovData(conditionalExpected, m_obs_nll, 0);
//  m_asimovData_0->Print("v");
}

void LQ3StatFormulae::CreateAsimovNLL()
{
    m_asimov_0_nll = createNLL(m_asimovData_0);
    m_map_snapshots[m_asimov_0_nll] = "conditionalGlobs_0";
    m_map_data_nll[m_asimovData_0] = m_asimov_0_nll;
    setMu(0);
    m_map_muhat[m_asimov_0_nll] = 0;
    saveSnapshot(m_asimov_0_nll, 0);
    m_workspace->loadSnapshot("conditionalNuis_0");
    m_workspace->loadSnapshot("conditionalGlobs_0");
    m_map_nll_muhat[m_asimov_0_nll] = m_asimov_0_nll->getVal();
    std::cout << "L." << __LINE__ <<  " : asimov_0_nll->getVal() = " << m_asimov_0_nll->getVal() << std::endl;

}
        
double LQ3StatFormulae::AsimovExpLimit(double initial_mu = 0) 
{ 
    double lim = getLimit(m_asimov_0_nll, initial_mu); 
    if ( !doExp ) lim = 1;
    return lim;
}
    
double LQ3StatFormulae::ObsLimit(double med_limit = 0) 
{
    double lim = getLimit(m_obs_nll, med_limit);       
    if ( doBlind ) lim = 0;
    return lim;
}      

double LQ3StatFormulae::getLimit(RooNLLVar* nll, double initial_guess)
{
    std::cout << "------------------------" << std::endl;
    std::cout << "Getting limit for nll: " << nll->GetName() << " with initial_guess = " << initial_guess << std::endl;
    //get initial guess based on muhat and sigma(muhat)
    m_POI->setConstant(0);
    global_status=0;

    if (nll == m_asimov_0_nll) {
        setMu(0);
        m_POI->setConstant(1);
    }

    double muhat;
    if (m_map_nll_muhat.find(nll) == m_map_nll_muhat.end()) {
        std::cout << "L." << __LINE__ <<  " : map_nll_muhat doesn't include the nll = " << nll->GetName() << std::endl;
        double nll_val = getNLL(nll);
        muhat = m_POI->getVal();
        std::cout << "L." << __LINE__ <<  " : muhat = " << muhat << std::endl;
        saveSnapshot(nll, muhat);
        m_map_muhat[nll] = muhat;
        if (muhat < 0 && doTilde)
        {
            setMu(0);
            m_POI->setConstant(1);
            nll_val = getNLL(nll);
        }
        m_map_nll_muhat[nll] = nll_val;

    } else {
        muhat = m_map_muhat[nll];
        std::cout << "L." << __LINE__ <<  " : muhat = " << muhat << std::endl;
    }

    if (muhat < 0.1 || initial_guess != 0) setMu(initial_guess);
    double qmu,qmuA;
    std::cout << "L." << __LINE__ <<  " : m_POI->getVal() = " << m_POI->getVal() << std::endl;
    double sigma_guess = getSigma(m_asimov_0_nll, m_POI->getVal(), 0, qmu);
    double sigma_b     = sigma_guess;
    std::cout << "L." << __LINE__ << " : Calculate the mu_guess. Input arguments are sigma_guess=" << sigma_guess << ", sigma_b=" << sigma_b << ", muhat=" << muhat  << std::endl;
    double mu_guess    = findCrossing(sigma_guess, sigma_b, muhat);
    double pmu         = calcPmu     (qmu, sigma_b, mu_guess);
    double pb          = calcPb      (qmu, sigma_b, mu_guess);
    double CLs         = calcCLs     (qmu, sigma_b, mu_guess);
    double qmu95       = getQmu95    (sigma_b, mu_guess);
    setMu(mu_guess);

    std::cout << "Initial guess:  " << mu_guess << std::endl;
    std::cout << "Sigma(obs):     " << sigma_guess << std::endl;
    std::cout << "Sigma(mu,0):    " << sigma_b << std::endl;
    std::cout << "muhat:          " << muhat << std::endl;
    std::cout << "qmu95:          " << qmu95 << std::endl;
    std::cout << "qmu:            " << qmu << std::endl;
    std::cout << "pmu:            " << pmu << std::endl;
    std::cout << "1-pb:           " << pb << std::endl;
    std::cout << "CLs:            " << CLs << std::endl;
    std::cout << std::endl;

    int nrDamping = 1;
    std::map<double, double> guess_to_corr;
    double damping_factor = 1.0;
    //double damping_factor_pre = damping_factor;
    int nrItr = 0;
    double mu_pre = muhat;//mu_guess-10*precision*mu_guess;
    double mu_pre2 = muhat;
    while (fabs(mu_pre-mu_guess) > precision*mu_guess*direction) {
        std::cout << "----------------------" << std::endl;
        std::cout << "Starting iteration " << nrItr << " of " << nll->GetName() << std::endl;
        // do this to avoid comparing multiple minima in the conditional and unconditional fits
        if      (nrItr == 0)       loadSnapshot(nll, muhat);
        else if (usePredictiveFit) doPredictiveFit(nll, mu_pre2, mu_pre, mu_guess);
        else                       loadSnapshot(m_asimov_0_nll, mu_pre);

        sigma_guess=getSigma(nll, mu_guess, muhat, qmu);
        saveSnapshot(nll, mu_guess);


        if (nll != m_asimov_0_nll)
        {
            if (nrItr == 0) loadSnapshot(m_asimov_0_nll, m_map_nll_muhat[m_asimov_0_nll]);
            else if (usePredictiveFit) 
            {
                if (nrItr == 1) doPredictiveFit(nll, m_map_nll_muhat[m_asimov_0_nll], mu_pre, mu_guess);
                else doPredictiveFit(nll, mu_pre2, mu_pre, mu_guess);
            }
            else loadSnapshot(m_asimov_0_nll, mu_pre);

            sigma_b=getSigma(m_asimov_0_nll, mu_guess, 0, qmuA);
            saveSnapshot(m_asimov_0_nll, mu_guess);
        }
        else
        {
            sigma_b=sigma_guess;
            qmuA=qmu;
        }

        double corr = damping_factor*(mu_guess - findCrossing(sigma_guess, sigma_b, muhat));
        for (std::map<double, double>::iterator itr=guess_to_corr.begin();itr!=guess_to_corr.end();itr++)
        {
            if (fabs(itr->first - (mu_guess-corr)) < direction*mu_guess*0.02 && fabs(corr) > direction*mu_guess*precision) 
            {
                damping_factor *= 0.8;
                std::cout << "Changing damping factor to " << damping_factor << ", nrDamping = " << nrDamping << std::endl;
                if (nrDamping++ > 10)
                {
                    nrDamping = 1;
                    damping_factor = 1.0;
                }
                corr *= damping_factor;
                break;
            }
        }

        //subtract off the difference in the new and damped correction
        guess_to_corr[mu_guess] = corr;
        mu_pre2 = mu_pre;
        mu_pre = mu_guess;
        mu_guess -= corr;


        pmu = calcPmu(qmu, sigma_b, mu_pre);
        pb = calcPb(qmu, sigma_b, mu_pre);
        CLs = calcCLs(qmu, sigma_b, mu_pre);
        qmu95 = getQmu95(sigma_b, mu_pre);


        std::cout << "NLL:            " << nll->GetName() << std::endl;
        std::cout << "Previous guess: " << mu_pre << std::endl;
        std::cout << "Sigma(obs):     " << sigma_guess << std::endl;
        std::cout << "Sigma(mu,0):    " << sigma_b << std::endl;
        std::cout << "muhat:          " << muhat << std::endl;
        std::cout << "pmu:            " << pmu << std::endl;
        std::cout << "1-pb:           " << pb << std::endl;
        std::cout << "CLs:            " << CLs << std::endl;
        std::cout << "qmu95:          " << qmu95 << std::endl;
        std::cout << "qmu:            " << qmu << std::endl;
        std::cout << "qmuA0:          " << qmuA << std::endl;
        std::cout << "Precision:      " << direction*mu_guess*precision << std::endl;
        std::cout << "Correction:    "  << (-corr<0?" ":"") << -corr << std::endl;
        std::cout << "New guess:      " << mu_guess << std::endl;
        std::cout << std::endl;

        nrItr++;
        if (nrItr > 25) {
            std::cout << "Infinite loop detected in getLimit(). Please intervene." << std::endl;
            break;
        }
    }

    std::cout << "Found limit for nll " << nll->GetName() << ": " << mu_guess << std::endl;
    std::cout << "Finished in " << nrItr << " iterations." << std::endl;
    std::cout << std::endl;
    return mu_guess;
}


double LQ3StatFormulae::getSigma(RooNLLVar* nll, double mu, double muhat, double& qmu)
{
    qmu = getQmu(nll, mu);
    std::cout << ".L" << __LINE__ << " qmu=" << qmu << ", mu=" << mu << ", muhat=" << muhat << ", direction=" << direction << std::endl;

    /* 
     * Plrease note that nll is "Negative" Log-Likelihood.
     * Thus ex. -lnL(muhat) = nll_muhat
     * */
    if      (mu*direction < muhat){ std::cout << "L." << __LINE__ << " sigma=" << fabs(mu-muhat)/sqrt(qmu)                  << std::endl; return fabs(mu-muhat)/sqrt(qmu);}
    else if (muhat < 0 && doTilde){ std::cout << "L." << __LINE__ << " sigma=" << sqrt(mu*mu-2*mu*muhat*direction)/sqrt(qmu)<< std::endl; return sqrt(mu*mu-2*mu*muhat*direction)/sqrt(qmu);}
    else                          { std::cout << "L." << __LINE__ << " sigma=" << (mu-muhat)*direction/static_cast<double>(TMath::Sqrt(qmu))            << std::endl; return (mu-muhat)*direction/static_cast<double>(TMath::Sqrt(qmu));}
}

double LQ3StatFormulae::getQmu(RooNLLVar* nll, double mu)
{
    std::cout << "L."<< __LINE__ << " " << __func__ << " mu=" << mu << std::endl;
    double nll_muhat = m_map_nll_muhat[nll];
    std::cout << "L."<< __LINE__ << " " << __func__ << " Got the nll_muhat="<< nll_muhat << std::endl;
    bool isConst = m_POI->isConstant();
    m_POI->setConstant(1);
    setMu(mu);
    double nll_val = getNLL(nll);
    std::cout << "L."<< __LINE__ << " " << __func__ << " Got the nll_val="<< nll_val << std::endl;
    m_POI->setConstant(isConst);
    std::cout << "qmu = 2 * (" << nll_val << " - " << nll_muhat << ") = " << 2*(nll_val-nll_muhat) << std::endl;
    return 2*(nll_val-nll_muhat);
}

void LQ3StatFormulae::saveSnapshot(RooNLLVar* nll, double mu)
{
    std::stringstream snapshotName;
    snapshotName << nll->GetName() << "_" << mu;
    m_workspace->saveSnapshot(snapshotName.str().c_str(), (m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet()));
}

void LQ3StatFormulae::loadSnapshot(RooNLLVar* nll, double mu)
{
    std::stringstream snapshotName;
    snapshotName << nll->GetName() << "_" << mu;
    m_workspace->loadSnapshot(snapshotName.str().c_str());
}

void LQ3StatFormulae::doPredictiveFit(RooNLLVar* nll, double mu1, double mu2, double mu)
{
    if (fabs(mu2-mu) < direction*mu*precision*4)
    {
        loadSnapshot(nll, mu2);
        return;
    }

    //extrapolate to mu using mu1 and mu2 assuming nuis scale linear in mu
    const RooArgSet* nuis = m_model_config->GetNuisanceParameters();
    if(nuis != 0) {
        int nrNuis = nuis->getSize();
        double* theta_mu1 = new double[nrNuis];
        double* theta_mu2 = new double[nrNuis];

        TIterator* itr = nuis->createIterator();
        RooRealVar* var;
        int counter = 0;
        loadSnapshot(nll, mu1);
        while ((var = (RooRealVar*)itr->Next()))
        {
            theta_mu1[counter++] = var->getVal();
        }

        itr->Reset();
        counter = 0;
        loadSnapshot(nll, mu2);
        while ((var = (RooRealVar*)itr->Next()))
        {
            theta_mu2[counter++] = var->getVal();
        }

        itr->Reset();
        counter = 0;
        while ((var = (RooRealVar*)itr->Next()))
        {
            double m = (theta_mu2[counter] - theta_mu1[counter])/(mu2-mu1);
            double b = theta_mu2[counter] - m*mu2;
            double theta_extrap = m*mu+b;

            var->setVal(theta_extrap);
            counter++;
        }

        delete itr;
        delete[] theta_mu1;
        delete[] theta_mu2;
    }
}

RooNLLVar* LQ3StatFormulae::createNLL(RooDataSet* _data)
{
    const RooArgSet* nuis = m_model_config->GetNuisanceParameters();
    RooNLLVar* nll;

    TString nCPU_str = getenv("NCORE");
    int nCPU = nCPU_str.Atoi();

    if (nuis != 0)
        nll = (RooNLLVar*)m_model_config->GetPdf()->createNLL(*_data, RooFit::Constrain(*nuis), RooFit::NumCPU(nCPU,3), RooFit::Optimize(2), RooFit::Offset(true));
    else
        nll = (RooNLLVar*)m_model_config->GetPdf()->createNLL(*_data, RooFit::NumCPU(nCPU,3), RooFit::Optimize(2), RooFit::Offset(true));

    return nll;
}

double LQ3StatFormulae::getNLL(RooNLLVar* nll)
{
    std::string snapshotName = m_map_snapshots[nll];
    if (snapshotName != "") m_workspace->loadSnapshot(snapshotName.c_str());
    minimize(nll);
    std::cout << "L." << __LINE__ << " " << __func__ << "() : try to getVal()." << std::endl;
    double val = nll->getVal();
    m_workspace->loadSnapshot("nominalGlobs");
    return val;
}


double LQ3StatFormulae::findCrossing(double sigma_obs, double sigma, double muhat)
{
    std::cout << "findCrossing():" << std::endl;
    std::cout << "L." << __LINE__ <<  ": sigma_obs = " << sigma_obs << ", sigma = " << sigma << ", muhat = " << muhat << ", target_CLs = " << target_CLs << std::endl;
    double mu_guess = muhat + ROOT::Math::gaussian_quantile(1-target_CLs,1)*sigma_obs*direction;
    std::cout << "L." << __LINE__ <<  " : Current mu upper limit->mu_guess = " << mu_guess << " because (muhat + ROOT::Math::gaussian_quantile(1-target_CLs,1)*sigma_obs*direction)" <<   std::endl;
    int nrItr     = 0;
    int nrDamping = 1;

    std::map<double, double> guess_to_corr;
    double damping_factor = 1.0;
    double mu_pre = mu_guess - 10*mu_guess*precision;
    while (fabs(mu_guess-mu_pre) > direction*mu_guess*precision) {

        const double sigma_obs_extrap = sigma_obs;
        const double eps = 0;
        if (extrapolateSigma)
        {
            //map<double, double> map_mu_sigma = map_nll_mu_sigma[nll];
        }

        std::cout << "nrItr[" << nrItr << "] L." << __LINE__ 
            << " fabs{mu_guess(" << mu_guess << ")-mu_pre(" << mu_pre << "} = " << fabs(mu_guess-mu_pre) << " VS " 
            << " direction(" << direction << ")*mu_guess(" << mu_guess << ")*precision(" << precision << ") " <<  direction*mu_guess*precision << std::endl;
        mu_pre = mu_guess;

        double qmu95 = getQmu95(sigma, mu_guess);
        std::cout << "Calculated the qmu95=" << qmu95 << std::endl;
        double qmu;
        qmu = 1./sigma_obs_extrap/sigma_obs_extrap*(mu_guess-muhat)*(mu_guess-muhat);
        std::cout << "Calculated the qmu=" << qmu << std::endl;
        if (muhat < 0 && doTilde) qmu = 1./sigma_obs_extrap/sigma_obs_extrap*(mu_guess*mu_guess-2*mu_guess*muhat);

        double dqmu_dmu = 2*(mu_guess-muhat)/sigma_obs_extrap/sigma_obs_extrap - 2*qmu*eps;
        std::cout << "Calculated the dqmu_dmu=" << dqmu_dmu << std::endl;

        double corr = damping_factor*(qmu-qmu95)/dqmu_dmu;
        std::cout << "Calculated the corr=" << corr << std::endl;
        for (std::map<double, double>::iterator itr=guess_to_corr.begin();itr!=guess_to_corr.end();itr++)
        {
            if (fabs(itr->first - mu_guess) < direction*mu_guess*precision) 
            {
                damping_factor *= 0.8;
                if (nrDamping++ > 10)
                {
                    nrDamping = 1;
                    damping_factor = 1.0;
                }
                corr *= damping_factor;
                break;
            }
        }
        guess_to_corr[mu_guess] = corr;

        mu_guess = mu_guess - corr;
        std::cout << "L." << __LINE__ <<  " : mu_guess = " << mu_guess << " corr=" << corr << std::endl;
        nrItr++;
        if (nrItr > 100)
        {
            std::cout << "Infinite loop detected in findCrossing. Please intervene." << std::endl;
            exit(1);
        }
    }
    std::cout << "L." << __LINE__ <<  " : mu_guess = " << mu_guess << std::endl;

    return mu_guess;
}

void LQ3StatFormulae::setMu(double mu)
{
    if (mu != mu)
    {
        std::cout << "ERROR::POI gave nan. Please intervene." << std::endl;
        exit(1);
    }
    if (mu > 0 && m_POI->getMax() < mu) m_POI->setMax(2*mu);
    if (mu < 0 && m_POI->getMin() > mu) m_POI->setMin(2*mu);
    m_POI->setVal(mu);
}


double LQ3StatFormulae::getQmu95_brute(double sigma, double mu)
{
    double step_size = 0.001;
    double start = step_size;
    if (mu/sigma > 0.2) start = 0;
    for (double qmu=start;qmu<20;qmu+=step_size)
    {
        double CLs = calcCLs(qmu, sigma, mu);

        if (CLs < target_CLs) return qmu;
    }

    return 20;
}

double LQ3StatFormulae::getQmu95(double sigma, double mu)
{
    double qmu95 = 0;
    //no sane man would venture this far down into |mu/sigma|
    double target_N = ROOT::Math::gaussian_cdf(1-target_CLs,1);
    if (fabs(mu/sigma) < 0.25*target_N)
    {
        qmu95 = 5.83/target_N;
    }
    else
    {
        std::map<double, double> guess_to_corr;
        double qmu95_guess = pow(ROOT::Math::gaussian_quantile(1-target_CLs,1),2);
        int nrItr = 0;
        int nrDamping = 1;
        double damping_factor = 1.0;
        double qmu95_pre = qmu95_guess - 10*2*qmu95_guess*precision;
        while (fabs(qmu95_guess-qmu95_pre) > 2*qmu95_guess*precision)
        {
            qmu95_pre = qmu95_guess;

            double corr = damping_factor*(calcCLs(qmu95_guess, sigma, mu)-target_CLs)/calcDerCLs(qmu95_guess, sigma, mu);
            for (std::map<double, double>::iterator itr=guess_to_corr.begin();itr!=guess_to_corr.end();itr++)
            {
                if (fabs(itr->first - qmu95_guess) < 2*qmu95_guess*precision) 
                {
                    damping_factor *= 0.8;
                    if (nrDamping++ > 10)
                    {
                        nrDamping = 1;
                        damping_factor = 1.0;
                    }
                    corr *= damping_factor;
                }
            }

            guess_to_corr[qmu95_guess] = corr;
            qmu95_guess = qmu95_guess - corr;

            nrItr++;
            if (nrItr > 200)
            {
                std::cout << "Infinite loop detected in getQmu95. Please intervene." << std::endl;
                exit(1);
            }
        }
        qmu95 = qmu95_guess;
    }

    if (qmu95 != qmu95) 
    {
        qmu95 = getQmu95_brute(sigma, mu);
    }

    return qmu95;
}

double LQ3StatFormulae::calcCLs(double qmu_tilde, double sigma, double mu)
{
    double pmu = calcPmu(qmu_tilde, sigma, mu);
    double pb = calcPb(qmu_tilde, sigma, mu);
    if (verbose)
    {
        std::cout << "pmu = " << pmu << std::endl;
        std::cout << "pb = " << pb << std::endl;
    }
    if (pb == 1) return 0.5;
    return pmu/(1-pb);
}

double LQ3StatFormulae::calcPmu(double qmu, double sigma, double mu)
{
    double pmu;
    if (qmu < mu*mu/(sigma*sigma) || !doTilde)
    {
        pmu = 1-ROOT::Math::gaussian_cdf(sqrt(qmu));
    }
    else
    {
        pmu = 1-ROOT::Math::gaussian_cdf((qmu+mu*mu/(sigma*sigma))/(2*fabs(mu/sigma)));
    }
    return pmu;
}

double LQ3StatFormulae::calcPb(double qmu, double sigma, double mu)
{
    if (qmu < mu*mu/(sigma*sigma) || !doTilde)
    {
        return 1-ROOT::Math::gaussian_cdf(fabs(mu/sigma) - sqrt(qmu));
    }
    else
    {
        return 1-ROOT::Math::gaussian_cdf((mu*mu/(sigma*sigma) - qmu)/(2*fabs(mu/sigma)));
    }
}

double LQ3StatFormulae::calcDerCLs(double qmu, double sigma, double mu)
{
    double dpmu_dq = 0;
    double d1mpb_dq = 0;

    if (qmu < mu*mu/(sigma*sigma))
    {
        double zmu = sqrt(qmu);
        dpmu_dq = -1./(2*sqrt(qmu*2*TMath::Pi()))*exp(-zmu*zmu/2);
    }
    else 
    {
        double zmu = (qmu+mu*mu/(sigma*sigma))/(2*fabs(mu/sigma));
        dpmu_dq = -1./(2*fabs(mu/sigma))*1./(sqrt(2*TMath::Pi()))*exp(-zmu*zmu/2);
    }

    if (qmu < mu*mu/(sigma*sigma))
    {
        double zb = fabs(mu/sigma)-sqrt(qmu);
        d1mpb_dq = -1./(2*sqrt(qmu*2*TMath::Pi()))*exp(-zb*zb/2);
    }
    else
    {
        double zb = (mu*mu/(sigma*sigma) - qmu)/(2*fabs(mu/sigma));
        d1mpb_dq = -1./(2*fabs(mu/sigma))*1./(sqrt(2*TMath::Pi()))*exp(-zb*zb/2);
    }

    double pb = calcPb(qmu, sigma, mu);
    return dpmu_dq/(1-pb)-calcCLs(qmu, sigma, mu)/(1-pb)*d1mpb_dq;
}

int LQ3StatFormulae::minimize(RooNLLVar* nll)
{
    nrMinimize++;
    RooAbsReal* fcn = (RooAbsReal*)nll;
    return minimize(fcn);
}

int LQ3StatFormulae::minimize(RooAbsReal* fcn)
{
    static int nrItr = 0;
    std::cout << "Starting minimization. Using these global observables" << std::endl;
//    m_model_config->GetGlobalObservables()->Print("v");


    int printLevel = ROOT::Math::MinimizerOptions::DefaultPrintLevel();
    RooFit::MsgLevel msglevel = RooMsgService::instance().globalKillBelow();
    if (printLevel < 0) RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

    int strat = ROOT::Math::MinimizerOptions::DefaultStrategy();
    int save_strat = strat;
    RooMinimizer minim(*fcn);
    minim.setStrategy(strat);
    minim.setPrintLevel(printLevel);
    minim.optimizeConst(2);


    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << " status = " << status << std::endl;
    std::cout << __FILE__ << " " << __LINE__ << std::endl;


    //up the strategy
    if (status != 0 && status != 1 && strat < 2)
    {
        strat++;
        std::cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << std::endl;
        minim.setStrategy(strat);
        status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }
    std::cout << __FILE__ << " " << __LINE__ << std::endl;

    if (status != 0 && status != 1 && strat < 2)
    {
        strat++;
        std::cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << std::endl;
        minim.setStrategy(strat);
        status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    }
    std::cout << __FILE__ << " " << __LINE__ << std::endl;

    //cout << "status is " << status << endl;

    // //switch minuit version and try again
    if (status != 0 && status != 1)
    {
        std::string minType = ROOT::Math::MinimizerOptions::DefaultMinimizerType();
        std::string newMinType;
        if (minType == "Minuit2") newMinType = "Minuit";
        else newMinType = "Minuit2";

        std::cout << "Switching minuit type from " << minType << " to " << newMinType << std::endl;

        ROOT::Math::MinimizerOptions::SetDefaultMinimizer(newMinType.c_str());
        strat = ROOT::Math::MinimizerOptions::DefaultStrategy();
        minim.setStrategy(strat);

        status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());


        if (status != 0 && status != 1 && strat < 2)
        {
            strat++;
            std::cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << std::endl;
            minim.setStrategy(strat);
            status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
        }

        if (status != 0 && status != 1 && strat < 2)
        {
            strat++;
            std::cout << "Fit failed with status " << status << ". Retrying with strategy " << strat << std::endl;
            minim.setStrategy(strat);
            status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(), ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
        }

        ROOT::Math::MinimizerOptions::SetDefaultMinimizer(minType.c_str());
    }
    std::cout << __FILE__ << " " << __LINE__ << std::endl;

    if (status != 0 && status != 1)
    {
        nrItr++;
        if (nrItr > maxRetries)
        {
            nrItr = 0;
            global_status++;
            std::cout << "WARNING::Fit failure unresolved with status " << status << std::endl;
            return status;
        }
        else
        {
            if (nrItr == 0) // retry with mu=0 snapshot
            {
                m_workspace->loadSnapshot("conditionalNuis_0");
                return minimize(fcn);
            }
            else if (nrItr == 1) // retry with nominal snapshot
            {
                m_workspace->loadSnapshot("nominalNuis");
                return minimize(fcn);
            }
        }
    }
    std::cout << __FILE__ << " " << __LINE__ << std::endl;

    if (printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(save_strat);
    std::cout << __FILE__ << " " << __LINE__ << std::endl;


    if (nrItr != 0) std::cout << "Successful fit" << std::endl;
    std::cout << __FILE__ << " " << __LINE__ << std::endl;
    nrItr=0;
    std::cout << __FILE__ << " " << __LINE__ << std::endl;
    return status;
}

void LQ3StatFormulae::unfoldConstraints(RooArgSet& initial, RooArgSet& final, RooArgSet& obs, RooArgSet& nuis, int& counter)
{
    if (counter > 50)
    {
        std::cout << "ERROR::Couldn't unfold constraints!" << std::endl;
        std::cout << "Initial: " << std::endl;
        initial.Print("v");
        std::cout << std::endl;
        std::cout << "Final: " << std::endl;
        final.Print("v");
        exit(1);
    }
    TIterator* itr = initial.createIterator();
    RooAbsPdf* pdf;
    while ((pdf = (RooAbsPdf*)itr->Next()))
    {
        RooArgSet nuis_tmp = nuis;
        RooArgSet constraint_set(*pdf->getAllConstraints(obs, nuis_tmp, false));
        std::string className(pdf->ClassName());
        if (className != "RooGaussian" && className != "RooLognormal" && className != "RooGamma" && className != "RooPoisson" && className != "RooBifurGauss")
        {
            counter++;
            unfoldConstraints(constraint_set, final, obs, nuis, counter);
        }
        else
        {
            final.add(*pdf);
        }
    }
    delete itr;
}



RooDataSet* LQ3StatFormulae::makeAsimovData(bool doConditional, RooNLLVar* conditioning_nll, double mu_val, std::string* mu_str,std::string* mu_prof_str, double mu_val_profile, bool doFit, double mu_injection)
{
    if (mu_val_profile == -999) mu_val_profile = mu_val;


    std::cout << "Creating asimov data at mu = " << mu_val << ", profiling at mu = " << mu_val_profile << std::endl;

    //ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    //int strat = ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    //int printLevel = ROOT::Math::MinimizerOptions::DefaultPrintLevel();
    //ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(-1);
    //RooMinuit::SetMaxIterations(10000);
    //RooMinimizer::SetMaxFunctionCalls(10000);

    ////////////////////
    //make asimov data//
    ////////////////////
    RooAbsPdf* combPdf = m_model_config->GetPdf();

    int _printLevel = 0;

    std::stringstream muStr;
    muStr << std::setprecision(5);
    muStr << "_" << mu_val;
    if (mu_str) *mu_str = muStr.str();

    std::stringstream muStrProf;
    muStrProf << std::setprecision(5);
    muStrProf << "_" << mu_val_profile;
    if (mu_prof_str) *mu_prof_str = muStrProf.str();

    RooRealVar* mu = (RooRealVar*)m_model_config->GetParametersOfInterest()->first();//m_workspace->var("mu");
    mu->setVal(mu_val);

    RooArgSet mc_obs = *m_model_config->GetObservables();
    RooArgSet mc_globs = *m_model_config->GetGlobalObservables();
    RooArgSet mc_nuis = (m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet());

    //pair the nuisance parameter to the global observable
    RooArgSet mc_nuis_tmp = mc_nuis;
    RooArgList nui_list("ordered_nuis");
    RooArgList glob_list("ordered_globs");
    RooArgSet constraint_set_tmp(*combPdf->getAllConstraints(mc_obs, mc_nuis_tmp, false));
    RooArgSet constraint_set;
    int counter_tmp = 0;
    unfoldConstraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp, counter_tmp);

    TIterator* cIter = constraint_set.createIterator();
    RooAbsArg* arg;
    while ((arg = (RooAbsArg*)cIter->Next()))
    {
        RooAbsPdf* pdf = (RooAbsPdf*)arg;
        if (!pdf) continue;
        TIterator* nIter = mc_nuis.createIterator();
        RooRealVar* thisNui = NULL;
        RooAbsArg* nui_arg;
        while ((nui_arg = (RooAbsArg*)nIter->Next()))
        {
            if (pdf->dependsOn(*nui_arg))
            {
                thisNui = (RooRealVar*)nui_arg;
                break;
            }
        }
        delete nIter;

        //RooRealVar* thisNui = (RooRealVar*)pdf->getObservables();

        //need this incase the observable isn't fundamental. 
        //in this case, see which variable is dependent on the nuisance parameter and use that.
        RooArgSet* components = pdf->getComponents();
        components->remove(*pdf);
        if (components->getSize())
        {
            TIterator* itr1 = components->createIterator();
            RooAbsArg* arg1;
            while ((arg1 = (RooAbsArg*)itr1->Next()))
            {
                TIterator* itr2 = components->createIterator();
                RooAbsArg* arg2;
                while ((arg2 = (RooAbsArg*)itr2->Next()))
                {
                    if (arg1 == arg2) continue;
                    if (arg2->dependsOn(*arg1))
                    {
                        components->remove(*arg1);
                    }
                }
                delete itr2;
            }
            delete itr1;
        }
        if (components->getSize() > 1)
        {
            std::cout << "ERROR::Couldn't isolate proper nuisance parameter" << std::endl;
            return NULL;
        }
        else if (components->getSize() == 1)
        {
            thisNui = (RooRealVar*)components->first();
        }



        TIterator* gIter = mc_globs.createIterator();
        RooRealVar* thisGlob = NULL;
        RooAbsArg* glob_arg;
        while ((glob_arg = (RooAbsArg*)gIter->Next()))
        {
            if (pdf->dependsOn(*glob_arg))
            {
                thisGlob = (RooRealVar*)glob_arg;
                break;
            }
        }
        delete gIter;

        if (!thisNui || !thisGlob)
        {
            std::cout << "WARNING::Couldn't find nui or glob for constraint: " << pdf->GetName() << std::endl;
            //return;
            continue;
        }

        nui_list.add(*thisNui);
        glob_list.add(*thisGlob);
    }
    delete cIter;




    //save the snapshots of nominal parameters, but only if they're not already saved
    m_workspace->saveSnapshot("tmpGlobs",*m_model_config->GetGlobalObservables());
    m_workspace->saveSnapshot("tmpNuis",(m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet()));
    if (!m_workspace->loadSnapshot("nominalGlobs"))
    {
        std::cout << "nominalGlobs doesn't exist. Saving snapshot." << std::endl;
        m_workspace->saveSnapshot("nominalGlobs",*m_model_config->GetGlobalObservables());
    }
    else m_workspace->loadSnapshot("tmpGlobs");
    if (!m_workspace->loadSnapshot("nominalNuis"))
    {
        std::cout << "nominalNuis doesn't exist. Saving snapshot." << std::endl;
        m_workspace->saveSnapshot("nominalNuis",(m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet()));
    }
    else m_workspace->loadSnapshot("tmpNuis");

    RooArgSet nuiSet_tmp(nui_list);

    mu->setVal(mu_val_profile);
    mu->setConstant(1);
    //int status = 0;
    if (doConditional && doFit)
    {
        minimize(conditioning_nll);
    }
    mu->setConstant(0);
    mu->setVal(mu_val);



    //loop over the nui/glob list, grab the corresponding variable from the tmp ws, and set the glob to the value of the nui
    int nrNuis = nui_list.getSize();
    if (nrNuis != glob_list.getSize())
    {
        std::cout << "ERROR::nui_list.getSize() != glob_list.getSize()!" << std::endl;
        return NULL;
    }

    for (int i=0;i<nrNuis;i++)
    {
        RooRealVar* nui = (RooRealVar*)nui_list.at(i);
        RooRealVar* glob = (RooRealVar*)glob_list.at(i);


        glob->setVal(nui->getVal());
    }

    //save the snapshots of conditional parameters
    // cout << "Saving conditional snapshots" << endl;
    // cout << "Glob snapshot name = " << "conditionalGlobs"+muStrProf.str() << endl;
    // cout << "Nuis snapshot name = " << "conditionalNuis"+muStrProf.str() << endl;
    m_workspace->saveSnapshot(("conditionalGlobs"+muStrProf.str()).c_str(),*m_model_config->GetGlobalObservables());
    m_workspace->saveSnapshot(("conditionalNuis" +muStrProf.str()).c_str(),( m_model_config->GetNuisanceParameters() ? *m_model_config->GetNuisanceParameters() : RooArgSet()));

    if (!doConditional)
    {
        m_workspace->loadSnapshot("nominalGlobs");
        m_workspace->loadSnapshot("nominalNuis");
    }

    //make the asimov data (snipped from Kyle)
    mu->setVal(mu_val);

    if(mu_injection > 0) {
        RooRealVar* norm_injection = m_workspace->var("ATLAS_norm_muInjection");
        if(norm_injection) {
            norm_injection->setVal(mu_injection);
        }
        else {
            mu->setVal(mu_injection);
        }
    }

    int iFrame=0;

    const char* weightName="weightVar";
    RooArgSet obsAndWeight;
    //cout << "adding obs" << endl;
    obsAndWeight.add(*m_model_config->GetObservables());
    //cout << "adding weight" << endl;

    RooRealVar* weightVar = NULL;
    if (!(weightVar = m_workspace->var(weightName)))
    {
        m_workspace->import(*(new RooRealVar(weightName, weightName, 1,0,10000000)));
        weightVar = m_workspace->var(weightName);
    }
    //cout << "weightVar: " << weightVar << endl;
    obsAndWeight.add(*m_workspace->var(weightName));

    //cout << "defining set" << endl;
    m_workspace->defineSet("obsAndWeight",obsAndWeight);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    // MAKE ASIMOV DATA FOR OBSERVABLES

    // dummy var can just have one bin since it's a dummy
    //if(m_workspace->var("ATLAS_dummyX"))  m_workspace->var("ATLAS_dummyX")->setBins(1);

    //cout <<" check expectedData by category"<<endl;
    //RooDataSet* simData=NULL;
    RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(m_model_config->GetPdf());

    RooDataSet* asimovData;
    if (!simPdf)
    {
        // Get pdf associated with state from simpdf
        RooAbsPdf* pdftmp = m_model_config->GetPdf();//simPdf->getPdf(channelCat->getLabel()) ;

        // Generate observables defined by the pdf associated with this state
        RooArgSet* obstmp = pdftmp->getObservables(*m_model_config->GetObservables()) ;

        asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),("asimovData"+muStr.str()).c_str(),RooArgSet(obsAndWeight),RooFit::WeightVar(*weightVar));

        RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
        double expectedEvents = pdftmp->expectedEvents(*obstmp);
        double thisNorm = 0;
        for(int jj=0; jj<thisObs->numBins(); ++jj){
            thisObs->setBin(jj);

            thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
            if (thisNorm*expectedEvents <= 0)
            {
                std::cout << "WARNING::Detected bin with zero expected events (" << thisNorm*expectedEvents << ") ! Please check your inputs. Obs = " << thisObs->GetName() << ", bin = " << jj << std::endl;
            }
            if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) asimovData->add(*m_model_config->GetObservables(), thisNorm*expectedEvents);
        }

        if(asimovData->sumEntries()!=asimovData->sumEntries()){
            std::cout << "sum entries is nan"<< std::endl;
            exit(1);
        }

        m_workspace->import(*asimovData);

    }
    else
    {
        std::map<std::string, RooDataSet*> asimovDataMap;


        //try fix for sim pdf
        RooCategory* channelCat = (RooCategory*)&simPdf->indexCat();//(RooCategory*)m_workspace->cat("master_channel");//(RooCategory*) (&simPdf->indexCat());
        //    TIterator* iter = simPdf->indexCat().typeIterator() ;
        TIterator* iter = channelCat->typeIterator() ;
        RooCatType* tt = NULL;
        int nrIndices = 0;
        while((tt=(RooCatType*) iter->Next())) {
            nrIndices++;
        }
        for (int i=0;i<nrIndices;i++){
            channelCat->setIndex(i);
            iFrame++;
            // Get pdf associated with state from simpdf
            RooAbsPdf* pdftmp = simPdf->getPdf(channelCat->getLabel()) ;

            // Generate observables defined by the pdf associated with this state
            RooArgSet* obstmp = pdftmp->getObservables(*m_model_config->GetObservables()) ;

            RooDataSet* obsDataUnbinned = new RooDataSet(Form("combAsimovData%d",iFrame),Form("combAsimovData%d",iFrame),RooArgSet(obsAndWeight,*channelCat),RooFit::WeightVar(*weightVar));
            RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
            double expectedEvents = pdftmp->expectedEvents(*obstmp);
            double thisNorm = 0;
            for(int jj=0; jj<thisObs->numBins(); ++jj){
                thisObs->setBin(jj);

                thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
                if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) obsDataUnbinned->add(*m_model_config->GetObservables(), thisNorm*expectedEvents);
            }

            if(obsDataUnbinned->sumEntries()!=obsDataUnbinned->sumEntries()){
                std::cout << "sum entries is nan"<< std::endl;
                exit(1);
            }

            asimovDataMap[std::string(channelCat->getLabel())] = obsDataUnbinned;//tempData;

        }

        asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),("asimovData"+muStr.str()).c_str(),RooArgSet(obsAndWeight,*channelCat),RooFit::Index(*channelCat),RooFit::Import(asimovDataMap),RooFit::WeightVar(*weightVar));
        m_workspace->import(*asimovData);
    }

    if(mu_injection > 0) {
        RooRealVar* norm_injection = m_workspace->var("ATLAS_norm_muInjection");
        if(norm_injection) {
            norm_injection->setVal(0);
        }
    }

    //bring us back to nominal for exporting
    //m_workspace->loadSnapshot("nominalNuis");
    m_workspace->loadSnapshot("nominalGlobs");

    return asimovData;
}

void LQ3StatFormulae::doExpected(bool isExpected) 
{
    if(isExpected) {
        doBlind               = 1;             // in case your analysis is blinded
        conditionalExpected   = 1 && !doBlind; // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
        doExp                 = 1;             // compute expected limit
        doObs                 = 1 && !doBlind; // compute observed limit
    }
    else{
        doBlind               = 0;             // in case your analysis is blinded
        conditionalExpected   = 1 && !doBlind; // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
        doExp                 = 1;             // compute expected limit
        doObs                 = 1 && !doBlind; // compute observed limit
    }
}

void LQ3StatFormulae::doBetterBands(bool isBetterBands) 
{
    betterBands = isBetterBands;
}

void LQ3StatFormulae::doInjection(bool injection) 
{
    doInj = injection;
}

void LQ3StatFormulae::Injection(double med_limit)
{
    double inj_limit = 0;
    int    inj_status=0;
    if(doInj) {

        std::cout << "Injected Mu " << mu_inj << std::endl;

        RooDataSet* asimovData_inj = makeAsimovData(conditionalExpected, m_obs_nll, 0, NULL, NULL, -999, true, mu_inj);

        int asimovinj_status=global_status;

        RooNLLVar* asimov_inj_nll = createNLL(asimovData_inj);//(RooNLLVar*)pdf->createNLL(*asimovData_0);
        m_map_snapshots[asimov_inj_nll] = "conditionalGlobs_0";
        m_map_data_nll[asimovData_inj] = asimov_inj_nll;
        setMu(0);
        m_workspace->loadSnapshot("conditionalNuis_0");
        m_workspace->loadSnapshot("conditionalGlobs_0");


        inj_limit = getLimit(asimov_inj_nll, med_limit);
        inj_status=global_status;
    }
}

void LQ3StatFormulae::BetterBands(double med_limit, double& mu_up_p1, double& mu_up_p2, double& mu_up_n1, double& mu_up_n2)
{
    double sigma = med_limit/sqrt(3.84); // pretty close

    //find quantiles, starting with +2, since we should be at +1.96 right now
    std::cout << "L." << __LINE__ <<  " : Find quantiles, starting with +2, since we should be at +1.96 right now." << std::endl;

    double init_targetCLs = getTargetCLs();
    std::cout << "L." << __LINE__ <<  " : init_targetCLs = " << init_targetCLs << std::endl;
    m_POI->setRange(-5*sigma, 5*sigma);
    for (int N=2;N>=-2;N--) {
        if (N < 0 && !betterNegativeBands) continue;
        if (N == 0) continue;
        target_CLs=2*(1-ROOT::Math::gaussian_cdf(fabs(N))); // change this so findCrossing looks for sqrt(qmu95)=2
        std::cout << "L." << __LINE__ <<  " : target_CLs = " << target_CLs << std::endl;
        if (N < 0) direction = -1;

        //get the acual value
        double NtimesSigma = getLimit(m_asimov_0_nll, N*med_limit/sqrt(3.84)); // use N * sigma(0) as an initial guess
        sigma = NtimesSigma/N;
        std::cout << "Found N * sigma = " << N << " * " << sigma << std::endl;

        std::string muStr,muStrPr;
        m_workspace->loadSnapshot("conditionalGlobs_0");
        double pr_val = NtimesSigma;
        if (N < 0 && profileNegativeAtZero) pr_val = 0;
        RooDataSet* asimovData_N = makeAsimovData(1, m_asimov_0_nll, NtimesSigma, &muStr, &muStrPr, pr_val, 0);

        RooNLLVar* asimov_N_nll = createNLL(asimovData_N);//(RooNLLVar*)pdf->createNLL(*asimovData_N);
        m_map_data_nll[asimovData_N] = asimov_N_nll;
        m_map_snapshots[asimov_N_nll] = "conditionalGlobs"+muStrPr;
        m_workspace->loadSnapshot(m_map_snapshots[asimov_N_nll].c_str());
        m_workspace->loadSnapshot(("conditionalNuis"+muStrPr).c_str());
        setMu(NtimesSigma);

        double nll_val = asimov_N_nll->getVal();
        saveSnapshot(asimov_N_nll, NtimesSigma);
        m_map_muhat[asimov_N_nll] = NtimesSigma;

        if (N < 0 && doTilde) {
            setMu(0);
            m_POI->setConstant(1);
            nll_val = getNLL(asimov_N_nll);
        }
        m_map_nll_muhat[asimov_N_nll] = nll_val;

        target_CLs = init_targetCLs;
        direction=1;
        double initial_guess = findCrossing(NtimesSigma/N, NtimesSigma/N, NtimesSigma);
        double limit = getLimit(asimov_N_nll, initial_guess);

        if      (N == 2) mu_up_p2 = limit;
        else if (N == 1) mu_up_p1 = limit;
        else if (N ==-1) mu_up_n1 = limit;
        else if (N ==-2) mu_up_n2 = limit;
    }
    direction = 1;
    target_CLs = init_targetCLs;
    m_workspace->loadSnapshot("conditionalNuis_0");
}
