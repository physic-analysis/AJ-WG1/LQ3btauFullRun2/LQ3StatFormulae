import subprocess

input_workspace = [
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_PrevConfig_Mtt0Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_PrevConfig_wEtmiss1stCut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_PrevConfig_wSt400Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_PrevConfig_wEtmiss1stCut_wSt400Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_Mtt0Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wEtmiss1stCut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wSt400Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wSt400Cut_wCollApprox_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wEtmiss1stCut_wSt400Cut_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wCollApprox_lephad_LQ3_BDT_{}',
#        'LeptoQuarkLepHad_22October2019.22October2019_LQ3_13TeV_22October2019_BDTScores_NewConfig_wEtmiss1stCut_wSt400Cut_wCollApprox_lephad_LQ3_BDT_{}',
#        "LeptoQuarkLepHad_15October2019.15October2019_LQ3_13TeV_15October2019_BasicKinematics_Base_TauPt100_sT_lephad_LQ3_BasicKinematics_Base_TauPt100_sT_{}"
#         'LeptoQuarkLepHad_syst_05January2020.syst_05January2020_LQ3_13TeV_syst_05January2020_BasicKinematics_Preselected_TauPT_lephad_LQ3_BasicKinematics_Preselected_TauPT_{}'

#         'LeptoQuarkLepHad_squirtle_20200119.squirtle_20200119_LQ3_13TeV_squirtle_20200119_BDTScore_config_prev_lephad_LQ3_BDT_{}',
#         'LeptoQuarkLepHad_squirtle_20200119.squirtle_20200119_LQ3_13TeV_squirtle_20200119_BDTScore_config_fullRun2_lephad_LQ3_BDT_{}'
         'LeptoQuarkLepHad_squirtle_mc16de.20200122.squirtle_mc16de.20200122_LQ3_13TeV_squirtle_mc16de.20200122_BDTScore_config_fullRun2_lephad_LQ3_BDT_{}',
         'LeptoQuarkLepHad_squirtle_mc16de.20200122.squirtle_mc16de.20200122_LQ3_13TeV_squirtle_mc16de.20200122_PNNScore_PNN_lephad_LQ3_{}'
        ]

for RooWorkspace in input_workspace:
    for mass in ('300','500','900','1300','1700'):
#    for mass in ('300',):
       subprocess.call(['AsymptoticCLs', RooWorkspace.format(mass), mass])


#for br in [i/100. for i in range(1,101)]:
#    for RooWorkspace in input_workspace:
#        for mass in ('300','500','900','1300'):
#           subprocess.call(['AsymptoticCLs', RooWorkspace.format(str(br).replace('.','p'),mass), mass])
#    
