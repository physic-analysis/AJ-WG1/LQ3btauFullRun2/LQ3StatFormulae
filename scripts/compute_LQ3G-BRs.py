#!/usr/bin/env python3

from __future__ import print_function

from math import pi, sqrt


def PrintBRCLs(aodlist, plot=False):
    """Print the branching ratio into charged leptons for each DSID."""
    dsids = []
    for aod in sorted(aods):
        fields = aod.split(".")
        dsid = int(fields[1])
        if dsid in dsids:
            continue
        mcconf = fields[2].split("_")
        lqtype = mcconf[2]
        masslq = int(mcconf[-1][1:])
        lmbda = float(mcconf[5].replace("p", "."))
        beta = float(mcconf[7].replace("p", "."))
        decaywidths = GetDecayWidths(lqtype, masslq, beta=beta, lmbda=lmbda)
        brcl = decaywidths[1] / sum(decaywidths.values())  # charged leptons
        brnl = decaywidths[0] / sum(decaywidths.values())  # neutral leptons
        print(str(dsid).ljust(10), round(brcl, 5))
        dsids.append(dsid)


def GetDecayWidths(lqtype, mlq, **kwargs):
    """Return a dictionary of decaywidths order w.r.t. the number of charged leptons in
       the final state."""
    beta = kwargs.get("beta", 0.5)
    lmbda = kwargs.get("lmbda", 0.3)
    # PDG data (2019):
    mtop = 172.9  # top quark mass
    mbottom = 4.18  # bottom quark mass
    mtau = 1.77686  # tau lepton mass
    decaywidths = {}  # key=0: uncharged, key=1: charged lepton
    if lqtype == "LQu":
        decaywidths[0] = ((mlq ** 2 - mtop ** 2) ** 2 * 3 * lmbda ** 2 * (1 - beta)) / (
            48 * pi * mlq ** 3
        )
        decaywidths[1] = (
            ((mlq ** 2 - mbottom ** 2 - mtau ** 2) * 3 * lmbda ** 2 * beta)
            / (48 * pi * mlq ** 3)
        ) * sqrt(
            mlq ** 4
            + mbottom ** 4
            + mtau ** 4
            - 2
            * (
                mlq ** 2 * mbottom ** 2
                + mlq ** 2 * mtau ** 2
                + mbottom ** 2 * mtau ** 2
            )
        )
    elif lqtype == "LQd":
        decaywidths[0] = (
            (mlq ** 2 - mbottom ** 2) ** 2 * 3 * lmbda ** 2 * (1 - beta)
        ) / (48 * pi * mlq ** 3)
        decaywidths[1] = (
            ((mlq ** 2 - mtop ** 2 - mtau ** 2) * 3 * lmbda ** 2 * beta)
            / (48 * pi * mlq ** 3)
        ) * sqrt(
            mlq ** 4
            + mtop ** 4
            + mtau ** 4
            - 2 * (mlq ** 2 * mtop ** 2 + mlq ** 2 * mtau ** 2 + mtop ** 2 * mtau ** 2)
        )
    else:
        print("ERROR: Unknown LQ type '{0}'!".format(lqtype))
    return decaywidths


if __name__ == "__main__":

    import sys
    from argparse import ArgumentParser

    parser = ArgumentParser("LQ3G-BR Calculator")
    parser.add_argument(
        "aod",
        metavar="AOD",
        type=str,
        nargs="+",
        help="AOD or .txt file conaining them",
    )
    args = vars(parser.parse_args())

    aods = []
    for arg in args["aod"]:
        if arg.endswith(".txt"):
            for line in open(arg).read().splitlines():
                if line.startswith("#"):
                    continue
                aods.append(line)
        else:
            aods.append(arg)

    if not all([item.startswith("mc16_13TeV") for item in aods]):
        print("ERROR: Arguments must be AODs or a .txt file containing them!")
        sys.exit(1)

    # Print branching ratio for a 3rd generation LQ to decay into
    # a **charged** lepton and a quark:
    PrintBRCLs(aods)
