

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"
#include "TFile.h"
#include "TH1D.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TString.h"

// RooStats, RooFit
#include "RooNLLVar.h"
#include "RooMinimizer.h"
#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooRealVar.h"
#include "Math/MinimizerOptions.h"
#include "TStopwatch.h"
#include "RooMinimizerFcn.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooProduct.h"
#include "RooPlot.h"
#include "TFile.h"

#include <map>
#include <iostream>
#include <sstream>
#include <iomanip>

class LQ3StatFormulae
{
    public:
        LQ3StatFormulae();
        LQ3StatFormulae(TString,TString, TString, TString);
        ~LQ3StatFormulae();

        void doExpected(bool isExpected);
        void doBetterBands(bool isBetterBands);
        void doInjection(bool injection);

        double      getLimit(RooNLLVar* nll, double initial_guess = 0);
        double      AsimovExpLimit(double);
        double      ObsLimit(double);      
        
        double      getSigma(RooNLLVar* nll, double mu, double muhat, double& qmu);
        double      getQmu(RooNLLVar* nll, double mu);
        void        saveSnapshot(RooNLLVar* nll, double mu);
        void        loadSnapshot(RooNLLVar* nll, double mu);
        void        doPredictiveFit(RooNLLVar* nll, double mu1, double m2, double mu);
        RooNLLVar*  createNLL(RooDataSet* _data);
        double      getNLL(RooNLLVar* nll);
        double      findCrossing(double sigma_obs, double sigma, double muhat);
        void        setMu(double mu);
        double      getQmu95_brute(double sigma, double mu);
        double      getQmu95(double sigma, double mu);
        double      calcCLs(double qmu_tilde, double sigma, double mu);
        double      calcPmu(double qmu_tilde, double sigma, double mu);
        double      calcPb(double qmu_tilde, double sigma, double mu);
        double      calcDerCLs(double qmu, double sigma, double mu);
        int         minimize(RooNLLVar* nll);
        int         minimize(RooAbsReal* nll);
        void        unfoldConstraints(RooArgSet& initial, RooArgSet& final, RooArgSet& obs, RooArgSet& nuis, int& counter);
        RooDataSet* makeAsimovData(bool doConditional, RooNLLVar* conditioning_nll, double mu_val, std::string* mu_str = NULL, std::string* mu_prof_str = NULL, double mu_val_profile = -999, bool doFit = true, double mu_injection = -1);

        void CreateObservedNLL();
        void CreateAsimov();
        void CreateAsimovNLL();

        void BetterBands(double, double&, double&, double&, double&);

        void Injection(double med_limit);
        RooDataSet*            getDataSet()    { return m_dataset;      }
        RooRealVar*            getPOI()        { return m_POI;          }
        RooWorkspace*          getWorkspace()  { return m_workspace;    }
        RooStats::ModelConfig* getModelConfig(){ return m_model_config; }
        RooNLLVar*             getAsimov0NLL() { return m_asimov_0_nll; }
        RooNLLVar*             getObsNLL()     { return m_obs_nll;      }

        double                 getTargetCLs()  { return target_CLs;}

    private:
        RooWorkspace*                             m_workspace;
        RooStats::ModelConfig*                    m_model_config;
        RooDataSet*                               m_dataset;
        RooDataSet*                               m_asimovData_0;
        RooRealVar*                               m_POI;
        RooNLLVar*                                m_asimov_0_nll;
        RooNLLVar*                                m_obs_nll;
        std::map<RooNLLVar*, double>              m_map_nll_muhat;
        std::map<RooNLLVar*, double>              m_map_muhat;
        std::map<RooDataSet*, RooNLLVar*>         m_map_data_nll;
        std::map<RooNLLVar*, std::string>              m_map_snapshots;
        std::map<RooNLLVar*, std::map<double, double>> map_nll_mu_sigma;

        int    nrMinimize;
        int    direction;
        int    global_status;
        double mu_inj;

        //band configuration
        bool betterBands           ; // (recommendation = 1) improve bands by using a more appropriate asimov dataset for those points
        bool betterNegativeBands   ; // (recommendation = 0) improve also the negative bands
        bool profileNegativeAtZero ; // (recommendation = 0) profile asimov for negative bands at zero

        //other configuration
        std::string defaultMinimizer    ; // or "Minuit"
        int defaultPrintLevel      ; // Minuit print level
        int defaultStrategy        ; // Minimization strategy. 0-2. 0 = fastest, least robust. 2 = slowest, most robust
        bool killBelowFatal        ; // In case you want to suppress RooFit warnings further, set to 1
        bool doBlind               ; // in case your analysis is blinded
        bool conditionalExpected   ; // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
        bool doTilde               ; // bound mu at zero if true and do the \tilde{q}_{mu} asymptotics
        bool doExp                 ; // compute expected limit
        bool doObs                 ; // compute observed limit
        bool doInj                 ; // compute expected limit after signal injection
        double precision           ; // % precision in mu that defines iterative cutoff
        bool verbose               ; // 1 = very spammy
        bool usePredictiveFit      ; // experimental, extrapolate best fit nuisance parameters based on previous fit results
        bool extrapolateSigma      ; // experimantal, extrapolate sigma based on previous fits
        int maxRetries             ; // number of minimize(fcn) retries before giving up
        double CL;
        double target_CLs;
};
