
#include "RooStats/HistFactory/Measurement.h"

void MakeSimpleModel(std::string input_dir, std::string input_file, std::string mass) {

    // Create a simple 1-channel model
    // using c++ and ROOT

    // Create the measurement object
    // This is the top node of the structure
    // We do some minor configuration as well
    RooStats::HistFactory::Measurement meas( "measurement", "measurement");

    // Set the prefix that will appear before
    // all output for this measurement
    // We Set ExportOnly to false, meaning
    // we will fit the measurement and make 
    // plots in addition to saving the workspace
    TString tmp = input_dir;
    tmp.ReplaceAll("input/","");
    tmp.ReplaceAll("normfiles/","");
    std::string output_prefix = "results/" + static_cast<std::string>(tmp) + "/LQ3Up";
    meas.SetOutputFilePrefix(output_prefix);
    meas.SetExportOnly(false);

    // Set the name of the parameter of interest
    // Note that this parameter hasn't yet been
    // created, we are anticipating it
    meas.SetPOI("SigXsecOverSM");

    // Set the luminosity
    // There are a few conventions for this.
    // Here, we assume that all histograms have
    // already been scaled by luminosity
    // We also set a 10% uncertainty
    meas.SetLumi(1);
    meas.SetLumiRelErr(0.1);

    // Okay, now that we've configured the measurement,
    // we'll start building the tree.
    // We begin by creating the first channel
    RooStats::HistFactory::Channel chan("channel");

    // First, we set the 'data' for this channel
    // The data is a histogram represeting the 
    // measured distribution.  It can have 1 or many bins.
    // In this example, we assume that the data histogram
    // is already made and saved in a ROOT file.  
    // So, to 'set the data', we give this channel the
    // path to that ROOT file and the name of the data
    // histogram in that root file
    // The arguments are: SetData(HistogramName, HistogramFile)
    //  std::string input_file = "input/Norm_BMin0_incJet1_dist1700_J2_DwZMassLowerCutSRLQ3BDT_T1_SpcTauLH_Y2015_LTT0_L1.root";
    std::string fullpath_input_file = input_dir + "/" + input_file;
    chan.SetData("data", fullpath_input_file );


    // Now that we have a channel and have attached
    // data to it, we will start creating our Samples
    // These describe the various processes that we
    // use to model the data.
    // Here, they just consist of a signal process
    // and a single background process.
    std::string signal_name = "LQ3Up" + mass;
    RooStats::HistFactory::Sample signal("signal", signal_name, fullpath_input_file);

    // Having created this sample, we configure it
    // First, we add the cross-section scaling
    // parameter that we call SigXsecOverSM
    // Then, we add a systematic with a 5% uncertainty
    // Finally, we add it to our channel
    double Low = -1000;
    double High = 1000;
    signal.AddNormFactor("SigXsecOverSM", 1, Low, High);
    signal.AddOverallSys("syst1",  0.95, 1.05);
    chan.AddSample(signal);     

    // We do a similar thing for our background
    std::vector<std::string> backgrounds =
    {
        "ttH",
        "VH",
        "Wtt",
        "W",
        "DYtt",
        "DY",
        "ttbar",
        "Fake",
        "Zttlf",
        "Zlf",
        "Zhf",
        "diboson",
        "Ztthf",
        "stop",
    };

    for ( unsigned int index =0; index<backgrounds.size(); index++ ){
        RooStats::HistFactory::Sample background( backgrounds.at(index), backgrounds.at(index), fullpath_input_file );
        //background.ActivateStatError("background1_statUncert", input_file );
        background.ActivateStatError();
        background.AddOverallSys("syst2", 0.95, 1.05 );
        chan.AddSample(background);         
    }

    // Now that we have fully configured our channel,
    // we add it to the main measurement
    meas.AddChannel(chan);

    // At this point, we have only given our channel
    // and measurement the input histograms as strings
    // We must now have the measurement open the files,
    // collect the histograms, copy and store them.
    // This step involves I/O 
    meas.CollectHistograms();

    // Print to the screen a text representation of the model
    // just for minor debugging
    meas.PrintTree();

    // Finally, run the measurement.
    // This is the same thing that happens when
    // one runs 'hist2workspace' on an xml files
    MakeModelAndMeasurementFast(meas);         

}

