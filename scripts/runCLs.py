import os
import subprocess

#lq_masses = ['300', '500', '900', '1300', '1700']
lq_masses = ['1700']
#cuts = [ 'wZMassLowerCut','woZMassLowerCut','woZMassLowerCut_wEtmissCut','NewVarSet_wEtmissCut', 'NewVarSet_woEtmissCut']
cuts = [ 'wZMassLowerCut']
for Mass in lq_masses:
    for Cut in cuts:
        directory    = "LeptoQuarkLepHad_9thAug2019.9thAug2019_HH_13TeV_9thAug2019_" + Cut + "_lephad_LQ3_BDT_" + Mass + "/"
        results_dir  = "results/" + directory
        results_file = "LQ3Up_combined_measurement_model.root"
        command='runAsymptoticsCLs.C("' + results_dir + results_file + '","combined","ModelConfig","obsData","","output/' + directory + '/root-files/obs/", "'+ Mass + '", 0.95)'
        subprocess.call(["root", "-l", "-b", "-q", command])
        #subprocess.call(['ls', input_dir+input_file])

