import numpy

if __name__ == "__main__":

    # PDG data (2019):
    m_top = 172.9  # top quark mass
    m_bottom = 4.18  # bottom quark mass
    m_tau = 1.77686  # tau lepton mass
    lmbda = 0.3
    beta = 0.5
    
    for m_lq in (300, 500, 900, 1300, 1700):
        decaywidths = {}
        decaywidths[0] = ((m_lq ** 2 - m_top ** 2) ** 2 * 3 * lmbda ** 2 * (1 - beta)) / (
            48 * numpy.pi * m_lq ** 3
        )
        decaywidths[1] = (
                ((m_lq ** 2 - m_bottom ** 2 - m_tau ** 2) * 3 * lmbda ** 2 * beta)
                / (48 * numpy.pi * m_lq ** 3)
            ) * numpy.sqrt(
                m_lq ** 4
                + m_bottom ** 4
                + m_tau ** 4
                - 2
                * (
                    m_lq ** 2 * m_bottom ** 2
                    + m_lq ** 2 * m_tau ** 2
                    + m_bottom ** 2 * m_tau ** 2
                )
            )
       
        Br = decaywidths[1]/sum(decaywidths.values())
        weight = 1/Br**2
        print("Bhat_m_{}GeV = {} # BR={}".format(m_lq, weight, Br))
