#include "RooFit.h"
#include "LQ3StatFormulae.h"

int main(int argc, char* argv[])
{
    TString input = argv[1];
    TString mass  = argv[2];

    TString infile          = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/WSMaker_HH_bbtautau/output/" + input + "/workspaces/combined/" + mass + ".root";
    TString workspaceName   = "combined";
    TString modelConfigName = "ModelConfig";
    TString dataName        = "obsData";

    // Set the default minimizer
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(1);
    ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(1);

    // Initialize the LQ3StatFormulae instance
    LQ3StatFormulae* Lq3Stat = new LQ3StatFormulae(infile, workspaceName, modelConfigName, dataName);

    /*  Create the observed NLL  */
    std::cout << "L." << __LINE__ <<  " : Create the observed NLL." << std::endl;
    Lq3Stat->CreateObservedNLL();

    /*  Create the asimov dataset  */
    Lq3Stat->CreateAsimov();
    std::cout << "L." << __LINE__ <<  " : Create the asimovData NLL."  << std::endl;
    Lq3Stat->CreateAsimovNLL();

    /* Calculate the asimov set NLL with mu = 0 (background only hypo). */
    double med_limit = Lq3Stat->AsimovExpLimit(0.2);
    std::cout << "L." << __LINE__ <<  " : The calculation is finised. med_limit = " << med_limit << std::endl;

    Lq3Stat->Injection(med_limit);

    double sigma = med_limit/sqrt(3.84); // pretty close
    double mu_up_p2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - Lq3Stat->getTargetCLs()*ROOT::Math::gaussian_cdf( 2), 1) + 2);
    double mu_up_p1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - Lq3Stat->getTargetCLs()*ROOT::Math::gaussian_cdf( 1), 1) + 1);
    double mu_up_n1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - Lq3Stat->getTargetCLs()*ROOT::Math::gaussian_cdf(-1), 1) - 1);
    double mu_up_n2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - Lq3Stat->getTargetCLs()*ROOT::Math::gaussian_cdf(-2), 1) - 2);

    double mu_up_p2 = mu_up_p2_approx;
    double mu_up_p1 = mu_up_p1_approx;
    double mu_up_n1 = mu_up_n1_approx;
    double mu_up_n2 = mu_up_n2_approx;

    Lq3Stat->getPOI()->setRange(-5*sigma, 5*sigma);

    // no better time than now to do this 
    Lq3Stat->BetterBands(med_limit, mu_up_p1, mu_up_p2, mu_up_n1, mu_up_n2);

    std::cout << "L." << __LINE__ <<  " : Calculate the observed limit." << std::endl;
    double obs_limit = Lq3Stat->ObsLimit(med_limit);
    std::cout << "L." << __LINE__ <<  " : The calculation is finished. The observed limit = " << obs_limit << std::endl;

    //    bool hasFailures = false;
    //    if (obs_status != 0 || med_status != 0 || asimov0_status != 0 || inj_status != 0) hasFailures = true;
    //    for (std::map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++) {
    //        if (itr->second != 0) hasFailures = true;
    //    }
    //    if (hasFailures) {
    //        std::cout << "--------------------------------" << std::endl;
    //        std::cout << "Unresolved fit failures detected" << std::endl;
    //        std::cout << "Asimov0:  " << asimov0_status << std::endl;
    //        for (std::map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++)
    //        {
    //            std::cout << "+" << itr->first << "sigma:  " << itr->first << std::endl;
    //        }
    //        std::cout << "Median:   " << med_status << std::endl;
    //        std::cout << "Injected: " << inj_status << std::endl;
    //        std::cout << "Observed: " << obs_status << std::endl;
    //        std::cout << "--------------------------------" << std::endl;
    //    }

    std::cout << "+2sigma:  " << mu_up_p2_approx << std::endl;
    std::cout << "+1sigma:  " << mu_up_p1_approx << std::endl;
    std::cout << "-1sigma:  " << mu_up_n1_approx << std::endl;
    std::cout << "-2sigma:  " << mu_up_n2_approx << std::endl;

    std::cout << std::endl;
    std::cout << "Correct bands" << std::endl;
    std::cout << "+2sigma:  " << mu_up_p2 << std::endl;
    std::cout << "+1sigma:  " << mu_up_p1 << std::endl;
    std::cout << "-1sigma:  " << mu_up_n1 << std::endl;
    std::cout << "-2sigma:  " << mu_up_n2 << std::endl;

    //std::cout << "Injected: " << inj_limit << std::endl;
    std::cout << "Median:   " << med_limit << std::endl;
    std::cout << "Observed: " << obs_limit << std::endl;
    std::cout << std::endl;


    TString folder = "./AsymptoticOutput/" + input + "/";
    system(("mkdir -vp " + static_cast<std::string>(folder)).c_str());

    std::stringstream fileName;
    fileName << folder << "/" << mass << ".root";
    TFile fout(fileName.str().c_str(),"recreate");

    TH1D* h_lim = new TH1D("limit","limit",8,0,8);
    h_lim->SetBinContent(1, obs_limit);
    h_lim->SetBinContent(2, med_limit);
    h_lim->SetBinContent(3, mu_up_p2);
    h_lim->SetBinContent(4, mu_up_p1);
    h_lim->SetBinContent(5, mu_up_n1);
    h_lim->SetBinContent(6, mu_up_n2);
    // h_lim->SetBinContent(7, inj_limit);
    //    h_lim->SetBinContent(8, global_status);

    h_lim->GetXaxis()->SetBinLabel(1, "Observed");
    h_lim->GetXaxis()->SetBinLabel(2, "Expected");
    h_lim->GetXaxis()->SetBinLabel(3, "+2sigma");
    h_lim->GetXaxis()->SetBinLabel(4, "+1sigma");
    h_lim->GetXaxis()->SetBinLabel(5, "-1sigma");
    h_lim->GetXaxis()->SetBinLabel(6, "-2sigma");
    h_lim->GetXaxis()->SetBinLabel(7, "Injected");
    h_lim->GetXaxis()->SetBinLabel(8, "Global status"); // do something with this later

    TH1D* h_lim_old = new TH1D("limit_old","limit_old",8,0,8); // include also old approximation of bands
    h_lim_old->SetBinContent(1, obs_limit);
    h_lim_old->SetBinContent(2, med_limit);
    h_lim_old->SetBinContent(3, mu_up_p2_approx);
    h_lim_old->SetBinContent(4, mu_up_p1_approx);
    h_lim_old->SetBinContent(5, mu_up_n1_approx);
    h_lim_old->SetBinContent(6, mu_up_n2_approx);
    //    h_lim_old->SetBinContent(7, inj_limit);
    //    h_lim_old->SetBinContent(8, global_status);

    h_lim_old->GetXaxis()->SetBinLabel(1, "Observed");
    h_lim_old->GetXaxis()->SetBinLabel(2, "Expected");
    h_lim_old->GetXaxis()->SetBinLabel(3, "+2sigma");
    h_lim_old->GetXaxis()->SetBinLabel(4, "+1sigma");
    h_lim_old->GetXaxis()->SetBinLabel(5, "-1sigma");
    h_lim_old->GetXaxis()->SetBinLabel(6, "-2sigma");
    h_lim_old->GetXaxis()->SetBinLabel(7, "Injected");
    h_lim_old->GetXaxis()->SetBinLabel(8, "Global status"); 

    //    std::cout << "Finished with " << nrMinimize << " calls to minimize(nll)" << std::endl;
    fout.Write();
    fout.Close();

    return 0;
}
