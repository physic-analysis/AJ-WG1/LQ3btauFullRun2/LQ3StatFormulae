
#include "RooStats/ModelConfig.h"

RooDataSet* makeAsimovData(bool doConditional, RooNLLVar* conditioning_nll, double mu_val, string* mu_str = NULL, string* mu_prof_str = NULL, double mu_val_profile = -999, bool doFit = true, double mu_injection = -1);

void StudyRooWS()
{
    TString path0 =  "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/WSMaker_HH_bbtautau/";
    //TString path1 = "output/LeptoQuarkLepHad_9thAug2019.9thAug2019_HH_13TeV_9thAug2019_NewVarSet_woEtmissCut_lephad_LQ3_BDT_1300/workspaces/combined/1300.root";
    TString path1 = "output/LeptoQuarkLepHad_9thAug2019.9thAug2019_HH_13TeV_9thAug2019_NewVarSet_woEtmissCut_lephad_LQ3_BDT_1700/workspaces/combined/1700.root";

    TFile* file = new TFile( path0 + path1, "read");
    RooWorkspace* ws = (RooWorkspace*)file->Get("combined");
    if ( !ws ) {
        std::cerr << " error "  << std::endl;
        return;
    }
    // ws->Print();

    RooStats::ModelConfig* mc = (RooStats::ModelConfig*)ws->obj("ModelConfig");
    const RooArgSet* nuis = mc->GetNuisanceParameters();
    mc->Print();

    // Get the created the asimov data set
    // mu = 0 (background only hypothesis)
    RooDataSet* dataset     = (RooDataSet*)ws->data("asimovData"); 
    if ( !dataset ){ std::cerr<< __LINE__ << std::endl; return;}
    std::cout << " ---- dataset Print ---- " << std::endl;
    dataset->Print("v");
    std::cout << " ------------------------ " << std::endl;

    RooDataSet* obs_dataset = (RooDataSet*)ws->data("obsData"); 
    if ( !obs_dataset ){ std::cerr<< __LINE__ << std::endl; return;}

    RooNLLVar* obs_nll = (RooNLLVar*)mc->GetPdf()->createNLL(*obs_dataset, RooFit::Constrain(*nuis), RooFit::NumCPU(3,3), RooFit::Optimize(2), RooFit::Offset(true));

    RooNLLVar* asimov_0_nll = (RooNLLVar*)mc->GetPdf()->createNLL(*dataset, RooFit::Constrain(*nuis), RooFit::NumCPU(3,3), RooFit::Optimize(2), RooFit::Offset(true));
    RooRealVar* SigXsecOverSM = (RooRealVar*)mc->GetParametersOfInterest()->first();
    SigXsecOverSM->setVal(0);
    //std::cout << "NLL muhat=" << asimov_0_nll->getVal() << std::endl;
    asimov_0_nll->Print();


    new TCanvas();
    RooRealVar* obs = ws->var("obs_x_Region_BMin0_incJet1_dist1700_J2_DNewVarSetwoEtmissCutSRLQ3BDT_T1_SpcTauLH_Y2015_LTT0_L1");
    RooPlot* xFrame = obs->frame();
    dataset->plotOn(xFrame);
    //c->SaveAs("test.png");
    //asimov_0_nll->plotOn(xFrame , RooFit::ShiftToZero());

    // RooRealVar* weightVar = ws->var("weightVar");
    // RooRealVar* obs_x     = ws->var("obs_x_Region_BMin0_incJet1_dist1700_J2_DNewVarSetwoEtmissCutSRLQ3BDT_T1_SpcTauLH_Y2015_LTT0_L1");
    // 
    // RooRealVar* mu  = ws->var("SigXsecOverSM");
    // RooPlot* xFrame = mu->frame();
    // dataset->plotOn(xframe);
    // xframe->Draw();
}

