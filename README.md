# How to use 
## Initialize the package
- source setup.sh
- mkdir build 
- cmake ..
- make -j8

## Run the asymptotic CLs method 
- python scripts/run.py
    - You should modify the input/output direcotry argument.

### NOTE
- The path to the input files can be changed in the src/AsymptoticCLs.cxx

## Draw the corss section limit
- python scripts/limit_lq.py
    - You should modify the inputdirecotry argument.


