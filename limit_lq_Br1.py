import sys
import ROOT as R
from array import array
from collections import OrderedDict as odict
import AtlasStyle as astyle

chan = "lephad"

def br_to_btau(m_lq, beta, lamb=0.3):
    m_b = 4.7
    m_tau=1.777
    return (m_lq**2 - m_b**2 - m_tau**2)*R.TMath.Sqrt(m_lq**4 + m_b**4 + m_tau**4 - 2*(m_lq**2*m_b**2 + m_lq**2*m_tau**2 + m_b**2*m_tau**2))*3.0*lamb**2*beta/(48*R.TMath.Pi()*m_lq**3)

def reweight_beta1(mass, br):
    weight = br #(br_to_btau(mass, beta=1.0)/br_to_btau(mass, beta=br))
    return 1/weight**2

def xsect(scale=False, chan="lephad", decay=False, stat=True, br = None, xstype = "old", scale1000 = False, rw = False):

    result = {}

    if xstype == "new":
        result = {
            200 : 64.5085,
            205 : 57.2279,
            210 : 50.9226,
            215 : 45.3761,
            220 : 40.5941,
            225 : 36.3818,
            230 : 32.6679,
            235 : 29.3155,
            240 : 26.4761,
            245 : 23.8853,
            250 : 21.5949,
            255 : 19.5614,
            260 : 17.6836,
            265 : 16.112,
            270 : 14.6459,
            275 : 13.3231,
            280 : 12.1575,
            285 : 11.0925,
            290 : 10.1363,
            295 : 9.29002,
            300 : 8.51615,
            305 : 7.81428,
            310 : 7.17876,
            315 : 6.60266,
            320 : 6.08444,
            325 : 5.60471,
            330 : 5.17188,
            335 : 4.77871,
            340 : 4.41629,
            345 : 4.08881,
            350 : 3.78661,
            355 : 3.50911,
            360 : 3.25619,
            365 : 3.02472,
            370 : 2.8077,
            375 : 2.61162,
            380 : 2.43031,
            385 : 2.26365,
            390 : 2.10786,
            395 : 1.9665,
            400 : 1.83537,
            405 : 1.70927,
            410 : 1.60378,
            415 : 1.49798,
            420 : 1.39688,
            425 : 1.31169,
            430 : 1.22589,
            435 : 1.14553,
            440 : 1.07484,
            445 : 1.01019,
            450 : 0.948333,
            455 : 0.890847,
            460 : 0.836762,
            465 : 0.787221,
            470 : 0.740549,
            475 : 0.697075,
            480 : 0.655954,
            485 : 0.618562,
            490 : 0.582467,
            495 : 0.549524,
            500 : 0.51848,
            505 : 0.489324,
            510 : 0.462439,
            515 : 0.436832,
            520 : 0.412828,
            525 : 0.390303,
            530 : 0.368755,
            535 : 0.348705,
            540 : 0.330157,
            545 : 0.312672,
            550 : 0.296128,
            555 : 0.280734,
            560 : 0.266138,
            565 : 0.251557,
            570 : 0.238537,
            575 : 0.226118,
            580 : 0.214557,
            585 : 0.203566,
            590 : 0.193079,
            595 : 0.183604,
            600 : 0.174599,
            605 : 0.166131,
            610 : 0.158242,
            615 : 0.150275,
            620 : 0.142787,
            625 : 0.136372,
            630 : 0.129886,
            635 : 0.123402,
            640 : 0.11795,
            645 : 0.112008,
            650 : 0.107045,
            655 : 0.102081,
            660 : 0.09725,
            665 : 0.0927515,
            670 : 0.0885084,
            675 : 0.0844877,
            680 : 0.0806192,
            685 : 0.0769099,
            690 : 0.0734901,
            695 : 0.0701805,
            700 : 0.0670476,
            705 : 0.0641426,
            710 : 0.0612942,
            715 : 0.0585678,
            720 : 0.0560753,
            725 : 0.0536438,
            730 : 0.0513219,
            735 : 0.0491001,
            740 : 0.0470801,
            745 : 0.045061,
            750 : 0.0431418,
            755 : 0.0413447,
            760 : 0.0396264,
            765 : 0.0379036,
            770 : 0.0363856,
            775 : 0.0348796,
            780 : 0.0334669,
            785 : 0.0320548,
            790 : 0.0307373,
            795 : 0.0295348,
            800 : 0.0283338,
            805 : 0.0272206,
            810 : 0.0261233,
            815 : 0.0251107,
            820 : 0.0241099,
            825 : 0.0230866,
            830 : 0.0221834,
            835 : 0.0213766,
            840 : 0.0204715,
            845 : 0.0197653,
            850 : 0.0189612,
            855 : 0.0182516,
            860 : 0.0175509,
            865 : 0.0168336,
            870 : 0.0162314,
            875 : 0.015625,
            880 : 0.0150143,
            885 : 0.0144112,
            890 : 0.0138979,
            895 : 0.0133962,
            900 : 0.0128895,
            905 : 0.0123843,
            910 : 0.0119837,
            915 : 0.0114713,
            920 : 0.0110688,
            925 : 0.0106631,
            930 : 0.0102629,
            935 : 0.0098874,
            940 : 0.00952142,
            945 : 0.00916636,
            950 : 0.00883465,
            955 : 0.00851073,
            960 : 0.00820884,
            965 : 0.00791403,
            970 : 0.00763112,
            975 : 0.00735655,
            980 : 0.00710317,
            985 : 0.00684867,
            990 : 0.00660695,
            995 : 0.00637546,
            1000 : 0.00615134,
            1005 : 0.00593765,
            1010 : 0.00572452,
            1015 : 0.00553094,
            1020 : 0.00533968,
            1025 : 0.00514619,
            1030 : 0.00497235,
            1035 : 0.00479906,
            1040 : 0.00463806,
            1045 : 0.00447537,
            1050 : 0.00432261,
            1055 : 0.00417983,
            1060 : 0.00403886,
            1065 : 0.0038962,
            1070 : 0.00376343,
            1075 : 0.00364174,
            1080 : 0.00352093,
            1085 : 0.00339813,
            1090 : 0.00328695,
            1095 : 0.00317628,
            1100 : 0.00307413,
            1105 : 0.00297377,
            1110 : 0.00287148,
            1115 : 0.00278078,
            1120 : 0.00268873,
            1125 : 0.00260821,
            1130 : 0.00251529,
            1135 : 0.00243484,
            1140 : 0.00236295,
            1145 : 0.00228192,
            1150 : 0.00221047,
            1155 : 0.00213907,
            1160 : 0.00206845,
            1165 : 0.0020063,
            1170 : 0.00194569,
            1175 : 0.0018741,
            1180 : 0.00182266,
            1185 : 0.00176211,
            1190 : 0.00170006,
            1195 : 0.00164968,
            1200 : 0.00159844,
            1205 : 0.0015472,
            1210 : 0.00149657,
            1215 : 0.00145544,
            1220 : 0.00140288,
            1225 : 0.00136155,
            1230 : 0.00131271,
            1235 : 0.0012717,
            1240 : 0.00123066,
            1245 : 0.00119994,
            1250 : 0.0011583,
            1255 : 0.00112694,
            1260 : 0.00108716,
            1265 : 0.00105517,
            1270 : 0.00102241,
            1275 : 0.000991293,
            1280 : 0.000961012,
            1285 : 0.000932394,
            1290 : 0.000903404,
            1295 : 0.000876957,
            1300 : 0.000850345,
            1305 : 0.00082443,
            1310 : 0.00079983,
            1315 : 0.000775222,
            1320 : 0.000751372,
            1325 : 0.000728912,
            1330 : 0.000706867,
            1335 : 0.000685372,
            1340 : 0.000664649,
            1345 : 0.000644804,
            1350 : 0.000625155,
            1355 : 0.000606802,
            1360 : 0.000588512,
            1365 : 0.000570506,
            1370 : 0.000553379,
            1375 : 0.000536646,
            1380 : 0.000521404,
            1385 : 0.000505008,
            1390 : 0.000490353,
            1395 : 0.000476164,
            1400 : 0.000461944,
            1405 : 0.000448172,
            1410 : 0.000435082,
            1415 : 0.000422967,
            1420 : 0.000410381,
            1425 : 0.000398106,
            1430 : 0.000386792,
            1435 : 0.000375724,
            1440 : 0.000364616,
            1445 : 0.000353965,
            1450 : 0.000343923,
            1455 : 0.000333885,
            1460 : 0.000324344,
            1465 : 0.0003153,
            1470 : 0.00030583,
            1475 : 0.000296811,
            1480 : 0.000288149,
            1485 : 0.000279711,
            1490 : 0.000271724,
            1495 : 0.000264275,
            1500 : 0.000256248
            }

    elif chan == "lephad" or chan == "tautau":

        #  XS below are for generating hh -> allall             305148    0.586E-02  1.0      1.0       LQ3btaubtau1000  Py8EG_A14NNPDF23LO_LQ3LQ3_1000_btaubtau

        xsectstr = """
        308043	5.8464e+01	1.0	1.0    lq3btaubtau200	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m200
        308044	7.8640e+00	1.0	1.0    lq3btaubtau300	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m300
        308045	1.7025e+00	1.0	1.0    lq3btaubtau400	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m400
        308046	4.8304e-01	1.0	1.0    lq3btaubtau500	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m500
        308047	1.6310e-01	1.0	1.0    lq3btaubtau600	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m600
        308048	6.1812e-02	1.0	1.0    lq3btaubtau700	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m700
        308049	2.5678e-02	1.0	1.0    lq3btaubtau800	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m800
        308050	1.1420e-02	1.0	1.0    lq3btaubtau900	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m900
        308051	5.3397e-03	1.0	1.0    lq3btaubtau1000	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1000
        308052	2.6091e-03	1.0	1.0    lq3btaubtau1100	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1100
        308053	1.3129e-03	1.0	1.0    lq3btaubtau1200	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1200
        308054	6.7894e-04	1.0	1.0    lq3btaubtau1300	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1300
        308055	3.5952e-04	1.0	1.0    lq3btaubtau1400	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1400
        308056	1.9645e-04	1.0	1.0    lq3btaubtau1500	 amcatnlopy8eg_a14n30nlo_lq3_up_beta1_m1500

        310550      7.91            1.0         1.0         LQ3beta05       aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M300
        310552      0.49            1.0         1.0         LQ3beta05       aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M500
        310554      0.0117          1.0         1.0         LQ3beta05       aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M900
        310556      0.000704        1.0         1.0         LQ3beta05       aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M1300
        310558      0.0000614       1.0         1.0         LQ3beta05       aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M1700

        """
             
    else:
        sys.exit("Unknown channel %s" % chan)

    BR = 1 

    scaling = {}
    if scale:
        f = open("scripts/HHbbtautau/xsratios8_13.txt")
        for l in f:
            tok = l.split()
            m = float(tok[0])
            r = float(tok[1])
            scaling[m] = r

    if xstype == "old":
        for l in xsectstr.split('\n'):
            tok = l.split()
            if not tok: continue        
            m = int(tok[-1].split("_")[5].replace("M", "").replace("hh", ""))
            xs = float(tok[1]) * BR

            result[m] = xs    

    for m, xs in result.iteritems():
        if not stat and (m in [1300,1400,1500] or (scale1000 and m == 1100) or (scale1000 and not rw and m in [900, 1000, 1100])): xs *= 1000

        if br is not None:
            xs *= reweight_beta1(m, br)

        if scale:
            xs /= scaling[m]

        result[m] = xs
        
    return result

def xsect2(scale=False, chan="hadhad", decay=False, stat=True):

    if chan != "lephad" and chan != "hadhad" and chan != "tautau":
        sys.exit("Unknown channel %s" % chan)

    #  XS below are for generating hh -> allall             305148    0.586E-02  1.0      1.0       LQ3btaubtau1000  Py8EG_A14NNPDF23LO_LQ3LQ3_1000_btaubtau
    xsectstr = """
    310550      7.91            1.0     1.0    LQ3beta05         aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M300
    310552      0.49            1.0     1.0    LQ3beta05         aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M500
    310554      0.0117          1.0     1.0    LQ3beta05         aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M900
    310556      0.000704        1.0     1.0    LQ3beta05         aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M1300
    310558      0.0000614       1.0     1.0    LQ3beta05         aMcAtNloPy8EG_OffDiag_LQ3_up_beta05_M1700
    """
    BR = 1 

    scaling = {}
    if scale:
        f = open("scripts/HHbbtautau/xsratios8_13.txt")
        for l in f:
            tok = l.split()
            m = float(tok[0])
            r = float(tok[1])
            scaling[m] = r

    result = {}
    
    for line in xsectstr.split('\n'):
        tok = line.split()
        if not tok: continue        
 
        mass = int(tok[-1].split("_")[5].replace("M", "").replace("hh", ""))
        cross_section = float(tok[1]) * BR

        # if not stat and mass in [300,1300,1400,1500]: cross_section *= 1000

        # tomoya add to consider branching ratio for beta=0.5
        if "beta05" in tok[4] : 
            if   mass == 300:  cross_section *= 0.4749; 
            elif mass == 500:  cross_section *= 0.31645;
            elif mass == 900:  cross_section *= 0.2689;  
            elif mass == 1300: cross_section *= 0.25889; 
            elif mass == 1700: cross_section *= 0.25516; 
        
        if scale:
            cross_section /= scaling[mass]

        result[mass] = cross_section
    
    return result


def LQXS(xstype = "old", br = None, decay = False):
    res = xsect(chan="lephad", decay = decay, br = br, xstype = xstype)

    m = array('f', [])
    e = array('f', [])

    for k, v in sorted(res.iteritems()):
#        if k < 400 or k > 1000: continue
        if k < 400 or k > 2000: continue
        m.append(k)
        e.append(v)

    #m = array('f', [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500])
    #e = array('f', [5.8464E+01, 7.8640E+00, 1.7025E+00, 4.8304E-01, 1.6310E-01, 6.1812E-02, 2.5678E-02, 1.1420E-02, 5.3397E-03, 2.6091E-03, 1.3129E-03, 
    #                6.7894E-04, 3.5952E-04, 1.9645E-04])

    return m, e
    
def plot(name, masses, same=False, color=R.kRed, legend="exp (LQ)", chan="lephad", decay=True, xstype = "new", br = None, scale1000 = False, rw = False):

    m = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    stat_only = "StatOnly" in name
    cross_section = xsect2(scale=False, chan=chan, decay=decay, stat = stat_only)

    for mass in masses:
        file_name = "build/AsymptoticOutput/" + name.format(mass) + "/" + str(mass) + ".root"
        input_file = R.TFile.Open(file_name)

        # Check the validity of the input file
        if input_file : 
            h_limit = input_file.Get("limit")

            if h_limit:
                # @@@@@@@@@@@@@@@@
                #  Blind Analysis
                # @@@@@@@@@@@@@@@@
                #  If you set some calculated value to the vo variable, this script show you the observed limit.
                #  Pleeeease be careful if you delete the comment out of this vo variable.
                # @@@@@@@@@@@@@@@@
                limit_xs_obs  = 0.0 #h_limit.GetBinContent(1) * cross_section[mass] 
                limit_xs_exp  = h_limit.GetBinContent(2)      * cross_section[mass] 
                vp1           = h_limit.GetBinContent(4)      * cross_section[mass] - limit_xs_exp
                vp2           = h_limit.GetBinContent(3)      * cross_section[mass] - limit_xs_exp
                vm1           = limit_xs_exp                  - h_limit.GetBinContent(5) * cross_section[mass]
                vm2           = limit_xs_exp                  - h_limit.GetBinContent(6) * cross_section[mass]
            else:
                print('@@@@@@@ There is no limit histograms in ' + file_name )
                limit_xs_obs = limit_xs_exp = vm1 = vp1 = vm2 = vp2 = 0
            
            input_file.Close()

        else :
            print ("@@@@@@@@@@ TFile opened fail : " + file_name )
            h_limit = None
            
        m.append(mass)
        o.append(limit_xs_obs)
        e.append(limit_xs_exp)
        m1.append(vm1)
        m2.append(vm2)
        p1.append(vp1)
        p2.append(vp2)

    print "-"*100
    print('   ' + legend)
    print "-"*100        
    print m
    print e
    
    z = array('f', [0]*len(m))

    g_expected = R.TGraphAsymmErrors(len(m), m, e, z, z, z, z)
    g_observed = R.TGraphAsymmErrors(len(m), m, o, z, z, z, z)
    g_1sigma   = R.TGraphAsymmErrors(len(m), m, e, z, z, m1, p1)
    g_2sigma   = R.TGraphAsymmErrors(len(m), m, e, z, z, m2, p2)

    # Set line & marker color
    g_expected.SetLineColor(color)
    g_expected.SetMarkerColor(color)
    g_observed.SetLineColor(color)
    g_observed.SetMarkerColor(color)
    ## Error bands
    g_1sigma.SetFillColor(R.kGreen)
    g_2sigma.SetFillColor(R.kYellow)

    if not same:
        g_expected.SetLineStyle(2)

    # Theory line
    mlq, elq = LQXS(xstype, decay)
    g_lq_theory = R.TGraph(len(mlq), mlq, elq)
    g_lq_theory.SetLineColor(R.kGreen+3)
    g_lq_theory.SetMarkerColor(R.kGreen+3)   

    # Main TGraph
    main_graph = R.TMultiGraph("", "Limits for " + chan + ";m(LQ_{3}^{u}) [GeV]; #sigma(pp #rightarrow LQ_{3}^{u}LQ_{3}^{u}) [pb]" )

    if not same:
        main_graph.Add(g_2sigma, "E3")
        main_graph.Add(g_1sigma, "E3")
        main_graph.Add(g_observed, "PL*")

    #main_graph.Add(g_expected, "PL*")
    main_graph.Add(g_expected, "L")
    
    if same:
        leg.AddEntry(g_expected, legend, "l")
    else:
        leg.AddEntry(g_lq_theory, 'Theory'      ,'l' )
        leg.AddEntry(g_expected , legend, "l")
        leg.AddEntry(g_1sigma   , '#pm 1 #sigma','f' )
        leg.AddEntry(g_2sigma   , '#pm 2 #sigma','f' )
        main_graph.Add(g_lq_theory, "L")

    R.gPad.SetLogy(True)
    #main_graph.Draw("P" if same else "AP")
    main_graph.Draw("L" if same else "AL")

    if not same:
        main_graph.GetYaxis().SetRangeUser(5e-4, 1)
    
    c._hists.append(main_graph)
    c._hists.append(g_expected)

    return m, e

def ratio2(m, exps, name="ratio.eps", bottom=False, title = "Ratio", legends = [], ymin = 0.2, ymax = 1.1):

    gm = R.TMultiGraph("", title + ";m_{X} [GeV]; Ratio of expected limits")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.58,0.60,0.89,0.79)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    
    nom = exps[0]
    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)        
        leg.AddEntry(g, legends[i] if legends else "new/old", "l")

        gm.SetMinimum(ymin)
        gm.SetMaximum(ymax)
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(250, 1, 1500, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    c.Print(name)


def ratio(masses, inputs, name="ratio.eps", bottom=False):
    """
        Create the ratio plot
    """

    m = array('f', masses)
    legs = inputs.keys()
    exps = inputs.values()

    gm = R.TMultiGraph("", "Limits for different setups ;m_{LQ} [GeV]; Ratio of expected limits for X/sT")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.58,0.60,0.89,0.79)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    nom = exps[0]

    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)
        leg.AddEntry(g, legs[i+1], "l")

    gm.SetMinimum(0.2)
    gm.SetMaximum(1.1)    
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(250, 1, 1000, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    c.Print(name)

    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)
    
    # ATLAS style 
    astyle.SetAtlasStyle()

    # Canvas
    c = R.TCanvas('c','c', 700,500)
    c.SetTopMargin(0.05)
    c.SetRightMargin(0.05)
    c.SetBottomMargin(0.16)
    c.SetLeftMargin(0.15)

    c._hists = []
    
    # Legend  
    leg = R.TLegend(0.5,0.6,0.93,0.8)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    outver = sys.argv[1] if len(sys.argv) > 1 else "150916"
    
    masses = [300,500,900,1300,1700]

    # Combined
    plot("LeptoQuarkLepHad_mc16a_5thSeptember2019.5thSeptember2019_LQ3_13TeV_5thSeptember2019_BDTScores_wZMassLowerCut_lephad_LQ3_BDT_{0}", masses, color=R.kBlack, chan="lephad", legend="36.1 /fb Exp 95% limit (prev w/ ZMassLowerCut)")
    plot("LeptoQuarkLepHad_2ndSeptember2019.2ndSeptember2019_LQ3_13TeV_2ndSeptember2019_BDTScores_wZMassLowerCut_lephad_LQ3_BDT_{0}"      , masses, color=R.kBlack, same = True, chan="lephad", legend="Exp 95% limit (prev w/ ZMassLowerCut)")
    plot("LeptoQuarkLepHad_2ndSeptember2019.2ndSeptember2019_LQ3_13TeV_2ndSeptember2019_BDTScores_NewVarSet_wEtmissCut_lephad_LQ3_BDT_{0}", masses, color=R.kRed, same = True, chan="lephad", legend="Exp 95% limit (new w/ EtmissCut)")

    #  
    #plot("LeptoQuarkLepHad_br1p0_6thSeptember2019.6thSeptember2019_LQ3_13TeV_6thSeptember2019_br1p0_BDTScores_wZMassLowerCut_lephad_LQ3_BDT_{0}", masses, color=R.kBlue, same = True, chan="lephad", legend="exp LQ_{lephad} New w/ EtmissCut")
    #plot("LeptoQuarkLepHad_br1p0_6thSeptember2019.6thSeptember2019_LQ3_13TeV_6thSeptember2019_br1p0_BDTScores_NewVarSet_wEtmissCut_lephad_LQ3_BDT_{0}", masses, color=R.kCyan, same = True, chan="lephad", legend="exp LQ_{lephad} New w/ EtmissCut")

    leg.Draw() 
    astyle.ATLASLabel(0.42,0.85,'Simulation Work in progress', c)
    c.Print("limit.LQ."+chan+".Up.pdf") 
    leg.Clear() 

    sys.exit()
