import ROOT


def NegativeLogLH(data_set, model_config) :
    nuis = model_config.GetNuisanceParameters();
    
    if nuis != 0 :
      nll = model_config.GetPdf().createNLL(data_set, ROOT.RooFit.Constrain(nuis))
    else :
      nll = model_config.GetPdf().createNLL(data_set)
    
    return nll

def makeAsimovData( workspace, model_config, conditioning_nll) : 
    
    weightName="weightVar"
    obsAndWeight = ROOT.RooArgSet()
    obsAndWeight.add(model_config.GetObservables());

    weightVar = ROOT.RooRealVar()
    if weightVar != workspace.var(weightName) :
#        workspace.import(ROOT.RooRealVar(weightName, weightName, 1,0,10000000))
        weightVar = workspace.var(weightName)
    obsAndWeight.add(workspace.var(weightName));

    workspace.defineSet("obsAndWeight",obsAndWeight);
   
    simultaneousPdf = ROOT.RooSimultaneous()
    simultaneousPdf = model_config.GetPdf()
    
    asimovDataMap = {}
    
    channelCat = simultaneousPdf.indexCat()
    iter = channelCat.typeIterator() ;
    nrIndices = 0;
    while iter.Next() : 
      nrIndices+=1

    for iCategory in range(0,nrIndices):
        
        channelCat.setIndex(iCategory);
    
        # Get pdf associated with state from simpdf
        pdf_category = simultaneousPdf.getPdf(channelCat.getLabel()) ;
          
        # Generate observables defined by the pdf associated with this state
        obs_category = pdf_category.getObservables(model_config.GetObservables()) ;
    
        obsDataUnbinned = ROOT.RooDataSet("combAsimovData{0}".format(iCategory), "combAsimovData{0}".format(iCategory), ROOT.RooArgSet(obsAndWeight,channelCat.getLabel()), ROOT.RooFit.WeightVar(weightVar));
        thisObs         = obs_category.first()
        expectedEvents  = pdf_category.expectedEvents(obs_category)
        print('Expected events = {0}'.format(expectedEvents))
        thisNorm = 0;
        for iBin in range(0, thisObs.numBins()) : 
            thisObs.setBin(iBin);
            thisNorm = pdf_category.getVal(obs_category)*thisObs.getBinWidth(iBin)
            
            if thisNorm*expectedEvents > 0 and thisNorm*expectedEvents < pow(10.0, 18) : 
                obsDataUnbinned.add(model_config.GetObservables(), thisNorm*expectedEvents)
        
        if obsDataUnbinned.sumEntries() != obsDataUnbinned.sumEntries() : 
          print("Sum entries is nan")
          exit(1);

        asimovDataMap[channelCat.getLabel()] = obsDataUnbinned
    
#    asimovData = ROOT.RooDataSet("asimovData","asimovData", ROOT.RooArgSet(obsAndWeight,*channelCat), ROOT.Index(channelCat), ROOT.Import(asimovDataMap), ROOT.RooFit.WeightVar(weightVar))
        print asimovDataMap
        print('Calculate the asimov')
        asimovData = ROOT.RooDataSet("asimovData","asimovData", ROOT.RooArgSet(obsAndWeight,channelCat.getLabel()), ROOT.RooFit.Index(channelCat),ROOT.RooFit.WeightVar(weightVar))
        print('Calculate the asimov : done')
   #     w->import(*asimovData);

    # bring us back to nominal for exporting
    # w->loadSnapshot("nominalNuis");
    workspace.loadSnapshot("nominalGlobs");
    
    print('Return the asimov')
    return asimovData;


def AsimovExpCLs() : 
    return 1

def setMu(POI, mu):
    '''
    Set the mu values
    '''
    if mu > 0 and POI.getMax() < mu : POI.setMax(2*mu);
    if mu < 0 and POI.getMin() > mu : POI.setMin(2*mu);
    POI.setVal(mu)

#def findCrossing
#def calcPmu     
#def calcPb      
#def calcCLs     
#def getQmu95    

def CalculateSigma(nll, mu, muhat, qmu):

    # Calculate the test statistics, qmu
    nll_muhat = nll.getVal()
    isConst   = POI.isConstant()
    POI.setConstant(1)
    setMu(POI, mu)
    nll_val = getNLL(nll)
    POI.setConstant(isConst)
    qmu = 2*(nll_val-nll_muhat)
    
    # Plrease note that nll is "Negative" Log-Likelihood.
    # Thus ex. -lnL(muhat) = nll_muhat
    return (mu-muhat)/sqrt(qmu)

def minimize(nll):
    strategy = ROOT.Math.MinimizerOptions.DefaultStrategy();
    save_strategy = strategy;
    minimizer = ROOT.RooMinimizer(nll);
    minimizer.setStrategy(strategy);
    minimizer.optimizeConst(2);
    
    status = minimizer.minimize(ROOT.Math.MinimizerOptions.DefaultMinimizerType(), ROOT.Math.MinimizerOptions.DefaultMinimizerAlgo());
    print("Status = {0}".format(status))
    
    ROOT.Math.MinimizerOptions.SetDefaultStrategy(save_strategy);
    
    return status

if __name__ == '__main__' :

    infile          = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/WSMaker_HH_bbtautau/output/LeptoQuarkLepHad_24thAug2019.24thAug2019_LQ3_13TeV_24thAug2019_BDTScores_wZMassLowerCut_lephad_LQ3_BDT_300/workspaces/combined/300.root'
    workspaceName   = 'combined'
    modelConfigName = 'ModelConfig'
    dataName        = 'obsData'
    
    # Set the default minimizer
    ROOT.Math.MinimizerOptions.SetDefaultMinimizer('Minuit2');
    ROOT.Math.MinimizerOptions.SetDefaultStrategy(1);
    ROOT.Math.MinimizerOptions.SetDefaultPrintLevel(1);
    
    # Open the RooWorkspace
    fin = ROOT.TFile.Open(infile);
    ws = fin.Get(workspaceName);
    if not ws: 
      print "ERROR::Workspace: " + workspaceName + " doesn't exist!"
      exit(1)
#    ws.Print()
    
    # Get the model config
    mc = ws.obj(modelConfigName);
    if not mc:
      print "ERROR::ModelConfig: " + modelConfigName + " doesn't exist!"
      exit(1)
    POI_mu = mc.GetParametersOfInterest().first()
    
    # Get the data set
    dataset = ws.data(dataName);
    if not dataset : 
      print("ERROR::Dataset: ", dataName, " doesn't exist!")
      exit(1)
    
    # Find injected mu before anything moves the PoI (for second case)
    doInj = 1
    if doInj :  
        if  ws.var("ATLAS_norm_muInjection") :
            mu_inj = ws.var("ATLAS_norm_muInjection").getVal();
        else :
            mu_inj = ws.var("SigXsecOverSM").getVal(); 
    
    # Create the observed NLL.
    obs_nll = NegativeLogLH(dataset, mc)
    
    #map_snapshots[obs_nll] = "nominalGlobs";
    #map_data_nll[dataset]  = obs_nll;
    #### w->saveSnapshot("nominalGlobs",*mc->GetGlobalObservables());
    #### w->saveSnapshot("nominalNuis",( mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet()));
    
    # *************************************** #
    # *     Create the asimov dataset       * #
    # *************************************** #
#    asimovDataName = 'asimovData'
    asimovDataName = ''
    asimovData_0 = ws.data(asimovDataName);
    if not asimovData_0 : 
        print("Asimov data doesn't exist, thus new asimov data set will be created.")
        asimovData_0 = makeAsimovData(ws, mc, obs_nll)
    
    asimovData_0.Print("v");
      
    # Calculate the asimov set NLL with mu = 0 (background only hypo). 
    print("Create the asimovData NLL.")
    #map_snapshots[asimov_0_nll] = "conditionalGlobs_0";
    #map_data_nll[asimovData_0]  = asimov_0_nll;
    #setMu(0);
    #map_muhat[asimov_0_nll] = 0;
    #saveSnapshot(asimov_0_nll, 0);
    #w->loadSnapshot("conditionalNuis_0");
    #w->loadSnapshot("conditionalGlobs_0");
    #map_nll_muhat[asimov_0_nll] = asimov_0_nll.getVal();
    #print("asimov_0_nll->getVal() = {0}".format(asimov_0_nll.getVal()))
    #
    #target_CLs=1-CL;
    
    #med_limit = getLimit(asimov_0_nll, 0.2)
    #print("The median(expected) limit is calculated. {0}".format(med_limit))

    # *************************************** #
    # *            Expected limit.          * #
    # *************************************** #
    # (1) Calculate the test statistics using the Asimov data set
    #   q_{mu,A} = -2 * ln(lambda(mu)) 
    #            = -2 * {  lnL(mu, theta^^) -   lnL(mu^,theta^) } 
    #            =  2 * { -lnL(mu, theta^^) - (-lnL(mu^,theta^)) } # ! RooStats return the nll
    
    ## (1-a) lnL(mu=0, theta)
    nll_asimov_muprime0 = NegativeLogLH(asimovData_0, mc)
    setMu(POI_mu, 0)
    POI_mu.setConstant(1)
    value_nll_muhat = nll_asimov_muprime0.getVal()
    POI_mu.setConstant(0)
    print("value_nll_muhat = {0}".format(value_nll_muhat))
    
    ## (1-b) lnL(mu, theta)
    initial_mu = 0.2
    isConstant = POI_mu.isConstant()
    POI_mu.setConstant(1)
    setMu(POI_mu, initial_mu)
    minimize(nll_asimov_muprime0);
    value_nll_mu = nll_asimov_muprime0.getVal()
    POI_mu.setConstant(isConstant)
    print('value_nll_mu = {0}'.format(value_nll_mu))
    
    q_muA = 2*(value_nll_mu - value_nll_muhat)
    print("Calculated q_muA = {0}".format(q_muA))

    # (2) sigma_{A}
    sigma_A = initial_mu/ROOT.Math.Sqrt(q_muA)
    print("Calculated sigma_A = {0}".format(sigma_A))

    # (3) Compute the upper limit for the mu 
    #   mu_{up} = mu^ + sigma_A * Quantile(1 - alpha)
    mu = value_nll_muhat + sigma_A * ROOT.Math.gaussian_quantile(1-0.05)
    print('Calculated mu = {0}'.format(mu))
    
    '''
    
      double inj_limit = 0;
      int inj_status=0;
    
      if(doInj)
      {
    //     double mu_inj = 1.;
    //     if ( w->var("ATLAS_norm_muInjection") ) {
    //       mu_inj = w->var("ATLAS_norm_muInjection")->getVal();
    //     } else {
    //       mu_inj = w->var("SigXsecOverSM")->getVal(); // This needs to be put in by hand for injected mass = test mass to be correct
    //     }
    
        std::cout << "Injected Mu " << mu_inj << std::endl;
        
        RooDataSet* asimovData_inj = makeAsimovData(conditionalExpected, obs_nll, 0, NULL, NULL, -999, true, mu_inj);
    
        int asimovinj_status=global_status;
    
        RooNLLVar* asimov_inj_nll = createNLL(asimovData_inj);//(RooNLLVar*)pdf->createNLL(*asimovData_0);
        map_snapshots[asimov_inj_nll] = "conditionalGlobs_0";
        map_data_nll[asimovData_inj] = asimov_inj_nll;
        setMu(0);
        w->loadSnapshot("conditionalNuis_0");
        w->loadSnapshot("conditionalGlobs_0");
    
        target_CLs=1-CL;
    
        inj_limit = getLimit(asimov_inj_nll, med_limit);
        inj_status=global_status;
      }
    
      double sigma = med_limit/sqrt(3.84); // pretty close
      double mu_up_p2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf( 2), 1) + 2);
      double mu_up_p1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf( 1), 1) + 1);
      double mu_up_n1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf(-1), 1) - 1);
      double mu_up_n2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf(-2), 1) - 2);
    
      double mu_up_p2 = mu_up_p2_approx;
      double mu_up_p1 = mu_up_p1_approx;
      double mu_up_n1 = mu_up_n1_approx;
      double mu_up_n2 = mu_up_n2_approx;
    
      firstPOI->setRange(-5*sigma, 5*sigma);
      map<int, int> N_status;
      if (betterBands && doExp) // no better time than now to do this
      {
        //find quantiles, starting with +2, since we should be at +1.96 right now
          std::cout << "L." << __LINE__ <<  " : Find quantiles, starting with +2, since we should be at +1.96 right now." << std::endl;
    
        double init_targetCLs = target_CLs;
          std::cout << "L." << __LINE__ <<  " : init_targetCLs = " << init_targetCLs << std::endl;
        firstPOI->setRange(-5*sigma, 5*sigma);
        for (int N=2;N>=-2;N--)
        {
          if (N < 0 && !betterNegativeBands) continue;
          if (N == 0) continue;
          target_CLs=2*(1-ROOT::Math::gaussian_cdf(fabs(N))); // change this so findCrossing looks for sqrt(qmu95)=2
          std::cout << "L." << __LINE__ <<  " : target_CLs = " << target_CLs << std::endl;
          if (N < 0) direction = -1;
    
          //get the acual value
          double NtimesSigma = getLimit(asimov_0_nll, N*med_limit/sqrt(3.84)); // use N * sigma(0) as an initial guess
          N_status[N] += global_status;
          sigma = NtimesSigma/N;
          cout << endl;
          cout << "Found N * sigma = " << N << " * " << sigma << endl;
    
          string muStr,muStrPr;
          w->loadSnapshot("conditionalGlobs_0");
          double pr_val = NtimesSigma;
          if (N < 0 && profileNegativeAtZero) pr_val = 0;
          RooDataSet* asimovData_N = makeAsimovData(1, asimov_0_nll, NtimesSigma, &muStr, &muStrPr, pr_val, 0);
          //RooDataSet* asimovData_N = makeAsimovData2(asimov_0_nll, NtimesSigma, pr_val, &muStr, &muStrPr);
    
    
          RooNLLVar* asimov_N_nll = createNLL(asimovData_N);//(RooNLLVar*)pdf->createNLL(*asimovData_N);
          map_data_nll[asimovData_N] = asimov_N_nll;
          map_snapshots[asimov_N_nll] = "conditionalGlobs"+muStrPr;
          w->loadSnapshot(map_snapshots[asimov_N_nll].c_str());
          w->loadSnapshot(("conditionalNuis"+muStrPr).c_str());
          setMu(NtimesSigma);
    
          double nll_val = asimov_N_nll->getVal();
          saveSnapshot(asimov_N_nll, NtimesSigma);
          map_muhat[asimov_N_nll] = NtimesSigma;
          if (N < 0 && doTilde)
          {
    	setMu(0);
    	firstPOI->setConstant(1);
    	nll_val = getNLL(asimov_N_nll);
          }
          map_nll_muhat[asimov_N_nll] = nll_val;
    
          target_CLs = init_targetCLs;
          direction=1;
          double initial_guess = findCrossing(NtimesSigma/N, NtimesSigma/N, NtimesSigma);
          double limit = getLimit(asimov_N_nll, initial_guess);
          N_status[N] += global_status;
    
          if (N == 2) mu_up_p2 = limit;
          else if (N == 1) mu_up_p1 = limit;
          else if (N ==-1) mu_up_n1 = limit;
          else if (N ==-2) mu_up_n2 = limit;
          //return;
        }
        direction = 1;
        target_CLs = init_targetCLs;
    
      }
    
      w->loadSnapshot("conditionalNuis_0");
      std::cout << "L." << __LINE__ <<  " : Calculate the observed limit." << std::endl;
      double obs_limit = doObs ? getLimit(obs_nll, med_limit) : 0;
      std::cout << "L." << __LINE__ <<  " : The calculation is finished. The observed limit = " << obs_limit << std::endl;
      int obs_status=global_status;
    
      bool hasFailures = false;
      if (obs_status != 0 || med_status != 0 || asimov0_status != 0 || inj_status != 0) hasFailures = true;
      for (map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++)
      {
        if (itr->second != 0) hasFailures = true;
      }
      if (hasFailures)
      {
        cout << "--------------------------------" << endl;
        cout << "Unresolved fit failures detected" << endl;
        cout << "Asimov0:  " << asimov0_status << endl;
        for (map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++)
        {
          cout << "+" << itr->first << "sigma:  " << itr->first << endl;
        }
        cout << "Median:   " << med_status << endl;
        cout << "Injected: " << inj_status << endl;
        cout << "Observed: " << obs_status << endl;
        cout << "--------------------------------" << endl;
      }
    
      if (betterBands) cout << "Guess for bands" << endl;
      cout << "+2sigma:  " << mu_up_p2_approx << endl;
      cout << "+1sigma:  " << mu_up_p1_approx << endl;
      cout << "-1sigma:  " << mu_up_n1_approx << endl;
      cout << "-2sigma:  " << mu_up_n2_approx << endl;
      if (betterBands)
      {
        cout << endl;
        cout << "Correct bands" << endl;
        cout << "+2sigma:  " << mu_up_p2 << endl;
        cout << "+1sigma:  " << mu_up_p1 << endl;
        cout << "-1sigma:  " << mu_up_n1 << endl;
        cout << "-2sigma:  " << mu_up_n2 << endl;
    
      }
    
      cout << "Injected: " << inj_limit << endl;
      cout << "Median:   " << med_limit << endl;
      cout << "Observed: " << obs_limit << endl;
      cout << endl;
    
    
      system(("mkdir -vp " + folder).c_str());
      
      stringstream fileName;
      fileName << folder << "/" << mass << ".root";
      TFile fout(fileName.str().c_str(),"recreate");
    
      TH1D* h_lim = new TH1D("limit","limit",8,0,8);
      h_lim->SetBinContent(1, obs_limit);
      h_lim->SetBinContent(2, med_limit);
      h_lim->SetBinContent(3, mu_up_p2);
      h_lim->SetBinContent(4, mu_up_p1);
      h_lim->SetBinContent(5, mu_up_n1);
      h_lim->SetBinContent(6, mu_up_n2);
      h_lim->SetBinContent(7, inj_limit);
      h_lim->SetBinContent(8, global_status);
    
      h_lim->GetXaxis()->SetBinLabel(1, "Observed");
      h_lim->GetXaxis()->SetBinLabel(2, "Expected");
      h_lim->GetXaxis()->SetBinLabel(3, "+2sigma");
      h_lim->GetXaxis()->SetBinLabel(4, "+1sigma");
      h_lim->GetXaxis()->SetBinLabel(5, "-1sigma");
      h_lim->GetXaxis()->SetBinLabel(6, "-2sigma");
      h_lim->GetXaxis()->SetBinLabel(7, "Injected");
      h_lim->GetXaxis()->SetBinLabel(8, "Global status"); // do something with this later
    
      TH1D* h_lim_old = new TH1D("limit_old","limit_old",8,0,8); // include also old approximation of bands
      h_lim_old->SetBinContent(1, obs_limit);
      h_lim_old->SetBinContent(2, med_limit);
      h_lim_old->SetBinContent(3, mu_up_p2_approx);
      h_lim_old->SetBinContent(4, mu_up_p1_approx);
      h_lim_old->SetBinContent(5, mu_up_n1_approx);
      h_lim_old->SetBinContent(6, mu_up_n2_approx);
      h_lim_old->SetBinContent(7, inj_limit);
      h_lim_old->SetBinContent(8, global_status);
    
      h_lim_old->GetXaxis()->SetBinLabel(1, "Observed");
      h_lim_old->GetXaxis()->SetBinLabel(2, "Expected");
      h_lim_old->GetXaxis()->SetBinLabel(3, "+2sigma");
      h_lim_old->GetXaxis()->SetBinLabel(4, "+1sigma");
      h_lim_old->GetXaxis()->SetBinLabel(5, "-1sigma");
      h_lim_old->GetXaxis()->SetBinLabel(6, "-2sigma");
      h_lim_old->GetXaxis()->SetBinLabel(7, "Injected");
      h_lim_old->GetXaxis()->SetBinLabel(8, "Global status"); 
    
      fout.Write();
      fout.Close();
    
      cout << "Finished with " << nrMinimize << " calls to minimize(nll)" << endl;
      '''
